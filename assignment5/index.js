
var gl,

    shapeWrapper,

    uTranslationVector,
    uRotationVector,

    uSampler0,
    uSampler1,

    uTextureFlag,
    uStaticColorFlag,

    imageLoaded0 = false,
    imageLoaded1 = false;

function init(){

    var program,

        cBufferId,
        vBufferId,
        tBufferId,

        aColor,
        aPosition,
        aTexCoord,

        texture0,
        texture1,

        uViewMatrix,
        uProjectionMatrix,

        eye = new Eye({position: {x: 0.0, y: 0.0, z: 4.0}}),

        image,
        imagePattern,
        texture,
        shape,

        cubeFactory = CubeFactoryCreator.getInstance(),
        sphereFactory = SphereFactoryCreator.getInstance(),

        canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.1, 0.1, 0.1, 1.0 );

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.POLYGON_OFFSET_FILL);
    gl.polygonOffset(1.0, 2.0);

    sphereFactory.set({radius: 1.0});

    //shape = cubeFactory.buildShape();
    shape = sphereFactory.buildShape();
    shapeWrapper = {
        shape: shape,
        offset: 0
    };

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the color data into the GPU
    cBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("colorArray")), gl.STATIC_DRAW );
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(shapeArray[shapeIndex].colors), gl.STATIC_DRAW );

    aColor = gl.getAttribLocation( program, "a_Color" );
    if (aColor < 0) {
        throw "Failed to get the storage location of a_Color";
    }
    gl.vertexAttribPointer( aColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aColor );

    // Load the data into the GPU
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("pointArray")), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    aPosition = gl.getAttribLocation( program, "a_Position" );
    if (aPosition < 0) {
        throw "Failed to get the storage location of a_Position";
    }
    gl.vertexAttribPointer( aPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aPosition );

    //texture buffer
    tBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, tBufferId);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("textureCoordinateArray")), gl.STATIC_DRAW );

    aTexCoord = gl.getAttribLocation( program, "a_TexCoord" );
    if (aTexCoord < 0) {
        throw "Failed to get the storage location of a_TexCoord";
    }
    gl.vertexAttribPointer( aTexCoord, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aTexCoord );

    uTextureFlag = gl.getUniformLocation(program, "u_TextureFlag");
    uStaticColorFlag = gl.getUniformLocation(program, "u_StaticColorFlag");

    uTranslationVector = gl.getUniformLocation(program, "u_TranslationVector");
    uRotationVector = gl.getUniformLocation(program, "u_RotationVector");

    uViewMatrix = gl.getUniformLocation( program, "u_ViewMatrix" );
    uProjectionMatrix = gl.getUniformLocation( program, "u_ProjectionMatrix" );

    gl.uniformMatrix4fv( uViewMatrix, false, flatten(eye.getLookAt()) );
    //gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(ortho(-1, 1, -1, 1, 5, -5)) );
    gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(perspective( 45, canvas.width/canvas.height, 0.1, 10 )) );

    gl.activeTexture( gl.TEXTURE0 );
    imagePattern = createImagePattern(256);
    texture0 = configureTexturePattern(imagePattern);


    //gl.bindTexture( gl.TEXTURE_2D, texture0 );
    gl.uniform1i(gl.getUniformLocation( program, "u_Sampler0"), 0);
    imageLoaded0 = true;

    image = new Image();
    image.src = "../images/terrian-map.jpg";
    image.onload = function(){

        gl.activeTexture( gl.TEXTURE1 );
        texture1 = configureImageTexture(this);


        //gl.bindTexture( gl.TEXTURE_2D, texture1 );
        gl.uniform1i(gl.getUniformLocation( program, "u_Sampler1"), 1);

        imageLoaded1 = true;
    };

    //init shape location
    shapeWrapper.shape.get('rotation').x = -20;
    shapeWrapper.shape.get('rotation').y = 180;
    shapeWrapper.shape.get('rotation').z = -20;
    updateShapeRotation();

}

function updateShapeTranslation(){

    gl.uniform3fv(uTranslationVector, glUtil.pointToArray(shapeWrapper.shape.get('translation')));
}

function updateShapeRotation(){

    gl.uniform3fv(uRotationVector, glUtil.pointToArray(shapeWrapper.shape.get('rotation')));
}

function createImagePattern(size){

    var c,
        patchx, patchy,
        numChecks = 16,
        image = new Uint8Array(4*size*size);

    for ( var i = 0; i < size; i++ ) {
        for ( var j = 0; j <size; j++ ) {

            patchx = Math.floor(i/(size/numChecks));
            patchy = Math.floor(j/(size/numChecks));

            if(patchx%2 ^ patchy%2){
                image[4*i*size+4*j] = 255;
                image[4*i*size+4*j+1] = 255;
                image[4*i*size+4*j+2] = 179;
            }
            else {
                image[4*i*size+4*j] = 251;
                image[4*i*size+4*j+1] = 128;
                image[4*i*size+4*j+2] = 114;
            }

            //c = 255*(((i & 0x8) == 0) ^ ((j & 0x8)  == 0))

            image[4*i*size+4*j+3] = 255;
        }
    }

    return {
        image: image,
        size: size
    };
}

function configureTexturePattern(imageWrapper) {

    var image = imageWrapper.image,
        size = imageWrapper.size,
        texture = gl.createTexture();

    gl.bindTexture( gl.TEXTURE_2D, texture );
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, size, size, 0, gl.RGBA, gl.UNSIGNED_BYTE, image);

    gl.generateMipmap( gl.TEXTURE_2D );

    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

    return texture;
}

function configureImageTexture(image) {

    var texture = gl.createTexture();
    gl.bindTexture( gl.TEXTURE_2D, texture );

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
    //gl.pixelStorei(gl.UNPACK_FLIP_X_WEBGL, 1);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR );

    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    //gl.generateMipmap( gl.TEXTURE_2D );

    //gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, image.width, image.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
}

function renderSphere(shapeWrapper){

    var shape = shapeWrapper.shape;

    //gl.uniform1i(uStaticColorFlag, 0);
    gl.drawArrays(gl.TRIANGLE_STRIP, shapeWrapper.offset, shape.get('bodyCount'));

    //gl.drawArrays(gl.TRIANGLE_STRIP, shapeWrapper.offset, shape.get('bodyCount'));
    //gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.offset + shape.get('topOffset'), shape.get('topCount'));
    //gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.offset + shape.get('bottomOffset'), shape.get('bottomCount'));

    //gl.drawArrays(gl.TRIANGLE_STRIP, 0, shape.get('bodyCount'));
    //gl.drawArrays(gl.TRIANGLE_FAN, shape.get('topOffset'), shape.get('topCount'));
    //gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));

    //if(i == shapeIndex){
    //    gl.uniform1i(uStaticColorFlag, 1);
    //}
    //else {
    //    gl.uniform1i(uStaticColorFlag, 0);
    //}

    //gl.uniform1i(uStaticColorFlag, 1);
    //gl.drawArrays(gl.LINE_STRIP, shapeWrapper.offset, shape.get('bodyCount'));
    //gl.drawArrays(gl.LINE_STRIP, shapeWrapper.offset + shape.get('topOffset'), shape.get('topCount'));
    //gl.drawArrays(gl.LINE_STRIP, shapeWrapper.offset + shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderCube(shapeWrapper){

    var shape = shapeWrapper.shape;

    //gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLES, shapeWrapper.offset, shape.get('pointArray').length);

    //if(i == shapeIndex){
    //    gl.uniform1i(uStaticColorFlag, 1);
    //}
    //else {
    //    gl.uniform1i(uStaticColorFlag, 0);
    //}
    //
    //gl.drawArrays(gl.LINES, shapeWrapper.offset, shape.get('pointArray').length);
}

function render(){

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //gl.drawArrays( gl.TRIANGLE_FAN, 0, 4 );

    switch(shapeWrapper.shape.get('type')) {
        case "cube":
            renderCube(shapeWrapper);
            break;
        case "sphere":
            renderSphere(shapeWrapper);
            break;
    }

}

function setupInput() {

    var canvas = document.getElementById("gl-canvas"),

        inputShapeTranslationX = document.getElementById("input-shape-translation-x"),
        inputShapeTranslationY = document.getElementById("input-shape-translation-y"),
        inputShapeTranslationZ = document.getElementById("input-shape-translation-z"),

        inputShapeRotationX = document.getElementById("input-shape-rotation-x"),
        inputShapeRotationY = document.getElementById("input-shape-rotation-y"),
        inputShapeRotationZ = document.getElementById("input-shape-rotation-z"),

        inputTextureArray = document.getElementsByName("input-texture"),

        initializeControls = function(){

            var shape = shapeWrapper.shape,

                shapeTranslation = shape.get('translation'),
                shapeRotation = shape.get('rotation');

            inputShapeTranslationX.value = shapeTranslation.x;
            inputShapeTranslationY.value = shapeTranslation.y;
            inputShapeTranslationZ.value = shapeTranslation.z;

            inputShapeRotationX.value = shapeRotation.x;
            inputShapeRotationY.value = shapeRotation.y;
            inputShapeRotationZ.value = shapeRotation.z;
        },

        inputTextureOnChange = function () {

            switch (this.value) {
                case 'pattern':
                    gl.uniform1i(uTextureFlag, 0);
                    break;
                case 'image':
                    gl.uniform1i(uTextureFlag, 1);
                    break;
                case 'combination':
                    gl.uniform1i(uTextureFlag, 2);
                    break;
                default:
                    break;
            }

            render();
        };

    for (i = 0; i < inputTextureArray.length; i++) {

        inputTextureArray[i].onchange = inputTextureOnChange;
    }

    //translation transformation input
    inputShapeTranslationX.oninput = function () {

        shapeWrapper.shape.get('translation').x = parseFloat(this.value);
        updateShapeTranslation();
        render();
    };

    inputShapeTranslationY.oninput = function () {

        shapeWrapper.shape.get('translation').y = parseFloat(this.value);
        updateShapeTranslation();
        render();
    };

    inputShapeTranslationZ.oninput = function () {

        shapeWrapper.shape.get('translation').z = parseFloat(this.value);
        updateShapeTranslation();
        render();
    };

    //rotation input callbacks
    inputShapeRotationX.oninput = function () {

        shapeWrapper.shape.get('rotation').x = parseFloat(this.value);
        updateShapeRotation();
        render();
    };

    inputShapeRotationY.oninput = function () {

        shapeWrapper.shape.get('rotation').y = parseFloat(this.value);
        updateShapeRotation();
        render();
    };

    inputShapeRotationZ.oninput = function () {

        shapeWrapper.shape.get('rotation').z = parseFloat(this.value);
        updateShapeRotation();
        render();
    };

}

function loadingLoop(){

    if(imageLoaded0 && imageLoaded1){
        render();
    }
    else {
        setTimeout(loadingLoop, 500);
    }
}

window.onload = function(){

    try {

        init();
        setupInput();
        loadingLoop();
        //render();
    }
    catch (e) {
        console.log(e);
    }
};