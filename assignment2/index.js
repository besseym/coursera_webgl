
var gl,
    offset = 0,
    cByteOffset = 0,
    vByteOffset = 0,
    lineWidth = 3,
    lineWidthArray = [],
    vCountArray = [],
    index = 0,
    vBufferId,
    cBufferId,
    doDraw = false,
    vertices = [],
    lineColor = [0, 0, 0],
    colors = [];

function init(){

    var program,
        vColor,
        vPosition,
        canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    //create and bind color buffer
    cBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, 8 * 512 * 512, gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );


    // create and bind vertex buffer
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, 8 * 512 * 512, gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );


    lineWidthArray.push(lineWidth);

    vCountArray.push(0);
}

function render() {

    var i = 0, vArray;

    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, cByteOffset, flatten(colors));

    gl.bindBuffer(gl.ARRAY_BUFFER, vBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, vByteOffset, flatten(vertices));

    for(i = 0; i < vCountArray.length - 1; i++){
        gl.lineWidth(lineWidthArray[i]);
        gl.drawArrays(gl.LINE_STRIP, vCountArray[i], vCountArray[i + 1] - vCountArray[i]);
    }

    gl.lineWidth(lineWidthArray[index]);
    gl.drawArrays(gl.LINE_STRIP, vCountArray[vCountArray.length - 1], vertices.length);
}

function setupInput() {

    var canvas = document.getElementById("gl-canvas"),
        inputLineWidth = document.getElementById("input-line-width"),
        outputLineWidth = document.getElementById("output-line-width"),
        inputColorRed = document.getElementById("input-color-red"),
        inputColorGreen = document.getElementById("input-color-blue"),
        inputColorBlue = document.getElementById("input-color-green"),
        outputColor = document.getElementById("output-color"),
        cWidth = canvas.width, cHeight = canvas.height,
        setOutColor = function(){

            var c = "rgb(" + parseInt(lineColor[0] * 256) + "," + parseInt(lineColor[1] * 256) + "," + parseInt(lineColor[2] * 256) + ")";
            outputColor.style.backgroundColor = c;
        },
        inputColorOnChange = function () {

            var v = parseFloat(this.value),
                c;

            switch(this.name){
                case 'input-color-red':
                    lineColor[0] = v;
                    break;
                case 'input-color-green':
                    lineColor[1] = v;
                    break;
                case 'input-color-blue':
                    lineColor[2] = v;
                    break;
            }

            setOutColor();
        },
        getClipCoordinaes = function(event){
            
            var x, y,
                xOff = event.offsetX,
                yOff = event.offsetY;

            if(!xOff || !yOff) {
                xOff = event.layerX - event.currentTarget.offsetLeft;
                yOff = event.layerY - event.currentTarget.offsetTop;
            }

            x = -1 + 2 * xOff / cWidth;
            y = -1 + 2 * (cHeight - yOff) / cHeight;

            return vec2(x, y);
        };

    setOutColor();

    inputLineWidth.onchange = function () {

        lineWidth = parseInt(this.value);
        outputLineWidth.value = lineWidth;
    };

    inputColorRed.onchange = inputColorOnChange;
    inputColorGreen.onchange = inputColorOnChange;
    inputColorBlue.onchange = inputColorOnChange;

    window.addEventListener("mousedown", function(event){

        doDraw = true;
        vertices = [];
        colors = [];

        lineWidthArray[index] = lineWidth;

    });

    canvas.addEventListener("mousemove", function(event){

        var clipCoordinaes;

        if(doDraw) {

            clipCoordinaes = getClipCoordinaes(event, canvas);

            vertices.push(clipCoordinaes);

            colors.push(lineColor);

            render();
        }
    });

    window.addEventListener("mouseup", function(event){

        doDraw = false;

        vCountArray.push(offset + vertices.length);

        cByteOffset = cByteOffset + colors.length * sizeof.vec3;
        vByteOffset = vByteOffset + vertices.length * sizeof.vec2;

        offset = offset + vertices.length;

        index++;

    });
}

window.onload = function(){

    try {

        init();
        setupInput();
    }
    catch (e) {
        console.log(e);
    }
};