
var gl,

    uTranslationVector,
    uRotationVector,
    uScaleVector,

    uViewMatrix,
    uIsStaticColor = 0,

    uLightIsEnabled0,
    uLightPosition0,
    uAmbient0,
    uDiffuse0,
    uSpecular0,
    uShininess0,

    uLightIsEnabled1,
    uLightPosition1,
    uAmbient1,
    uDiffuse1,
    uSpecular1,
    uShininess1,

    uStaticColorFlag = 0,

    lightIndex,
    lightArray = [],

    eye,
    shapeWrapper;

function init(){

    var program,

        nBufferId,
        vBufferId,

        aNormal,
        aPosition,

        uProjectionMatrix,

        canvas = document.getElementById( "gl-canvas"),

        cubeFactory = CubeFactoryCreator.getInstance(),
        coneFactory = ConeFactoryCreator.getInstance(),
        cylinderFactory = CylinderFactoryCreator.getInstance(),
        sphereFactory = SphereFactoryCreator.getInstance(),

        light,

        mValue,
        shape;

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );

    gl.enable(gl.DEPTH_TEST);
    //gl.depthFunc(gl.LEQUAL);
    //gl.enable(gl.POLYGON_OFFSET_FILL);
    //gl.polygonOffset(1.0, 2.0);

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    light = new Light({
        position: {x: 0.0, y: 0.0, z: -4.0},
        ambient: {r: 0.5, g: 0.5, b: 0.5, a: 1.0},
        diffuse: {r: 0.8, g: 0.8, b: 0.8, a: 1.0},
        specular: {r: 0.2, g: 0.2, b: 0.2, a: 1.0}
    });

    lightArray.push({
        light: light,
        isEnabled: true
    });

    light = new Light({
        position: {x: 2.0, y: 0.0, z: 0.0},
        ambient: {r: 0.5, g: 0.5, b: 0.5, a: 1.0},
        diffuse: {r: 0.8, g: 0.8, b: 0.8, a: 1.0},
        specular: {r: 0.2, g: 0.2, b: 0.2, a: 1.0}
    });

    lightArray.push({
        light: light,
        isEnabled: false
    });

    eye = new Eye({position: {x: 0.0, y: 0.0, z: 4.0}});

    //shape = cubeFactory.buildShape();
    //shape = coneFactory.buildShape();
    shape = cylinderFactory.buildShape();
    //shape = sphereFactory.buildShape();

    mValue = "pearl";
    shape.get("material").set(getMaterial(mValue));
    shapeWrapper = {
        shape: shape,
        material: mValue,
        materialLight: null
    };

    // Load the normal data into the GPU
    nBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, nBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("normalArray")), gl.STATIC_DRAW );

    aNormal = gl.getAttribLocation( program, "a_Normal" );
    gl.vertexAttribPointer( aNormal, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aNormal );

    // Load the vertex data into the GPU
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("pointArray")), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    aPosition = gl.getAttribLocation( program, "a_Position" );
    gl.vertexAttribPointer( aPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aPosition );


    uViewMatrix = gl.getUniformLocation( program, "u_ViewMatrix" );
    uProjectionMatrix = gl.getUniformLocation( program, "u_ProjectionMatrix" );

    uIsStaticColor = gl.getUniformLocation(program, "u_IsStaticColor");

    uTranslationVector = gl.getUniformLocation(program, "u_TranslationVector");
    uRotationVector = gl.getUniformLocation(program, "u_RotationVector");
    uScaleVector = gl.getUniformLocation(program, "u_ScaleVector");

    //light 0
    uLightIsEnabled0 = gl.getUniformLocation(program, "u_LightIsEnabled0");
    uLightPosition0 = gl.getUniformLocation(program, "u_LightPosition0");

    uAmbient0 = gl.getUniformLocation(program, "u_Ambient0");
    uDiffuse0 = gl.getUniformLocation(program, "u_Diffuse0");
    uSpecular0 = gl.getUniformLocation(program, "u_Specular0");
    uShininess0 = gl.getUniformLocation(program, "u_Shininess0");

    //light 1
    uLightIsEnabled1 = gl.getUniformLocation(program, "u_LightIsEnabled1");
    uLightPosition1 = gl.getUniformLocation(program, "u_LightPosition1");

    uAmbient1 = gl.getUniformLocation(program, "u_Ambient1");
    uDiffuse1 = gl.getUniformLocation(program, "u_Diffuse1");
    uSpecular1 = gl.getUniformLocation(program, "u_Specular1");
    uShininess1 = gl.getUniformLocation(program, "u_Shininess1");

    lightIndex = 0;
    updateLightStatus();
    updateLightPosition();
    updateCurrentLightProperties();

    lightIndex = 1;
    updateLightStatus();
    updateLightPosition();
    updateCurrentLightProperties();

    updateShapeTransformation();

    updateEyeLocation();
    //gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(ortho(-1, 1, -1, 1, 5, -5)) );
    gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(perspective( 45, canvas.width/canvas.height, 0.1, 10 )) );

}

function updateEyeLocation(){

    gl.uniformMatrix4fv( uViewMatrix, false, flatten(eye.getLookAt()) );
}

function updateShapeTransformation(){

    var shape = shapeWrapper.shape;

    gl.uniform3fv(uTranslationVector, glUtil.pointToArray(shape.get('translation')));
    gl.uniform3fv(uRotationVector, glUtil.pointToArray(shape.get('rotation')));
    gl.uniform3fv(uScaleVector, glUtil.pointToArray(shape.get('scale')));
}

function updateLightStatus(){

    var isEnabled = lightArray[lightIndex].isEnabled;

    if(lightIndex === 0) {
        gl.uniform1i(uLightIsEnabled0, isEnabled);
    }
    else if(lightIndex === 1) {
        gl.uniform1i(uLightIsEnabled1, isEnabled);
    }
}

function updateLightPosition(){

    var light = lightArray[lightIndex].light;

    if(lightIndex === 0) {
        gl.uniform3fv(uLightPosition0, flatten(glUtil.pointToArray(light.get("position"))));
    }
    else if(lightIndex === 1) {
        gl.uniform3fv(uLightPosition1, flatten(glUtil.pointToArray(light.get("position"))));
    }
}

function updateAllLightProperties(){

    var i = 0;

    for(i = 0; i < lightArray.length; i++){
        updateLightProperties(i);
    }
}

function updateCurrentLightProperties(){

    updateLightProperties(lightIndex);
}

function updateLightProperties(index){

    var lightWrapper = lightArray[index],
        light = lightWrapper.light,
        materialLight = shapeWrapper.shape.get("material").calculateLight(light);

    if(index === 0) {

        gl.uniform4fv(uAmbient0, flatten(materialLight.ambient));
        gl.uniform4fv(uDiffuse0, flatten(materialLight.diffuse));
        gl.uniform4fv(uSpecular0, flatten(materialLight.specular));
        gl.uniform1f(uShininess0, shapeWrapper.shape.get("material").get("shininess"));
    }
    else if(index === 1) {

        gl.uniform4fv(uAmbient1, flatten(materialLight.ambient));
        gl.uniform4fv(uDiffuse1, flatten(materialLight.diffuse));
        gl.uniform4fv(uSpecular1, flatten(materialLight.specular));
        gl.uniform1f(uShininess1, shapeWrapper.shape.get("material").get("shininess"));
    }
}

function renderCube(shape){

    gl.drawArrays( gl.TRIANGLES, 0, shape.get('pointArray').length);
}

function renderCone(shape){

    gl.drawArrays(gl.TRIANGLE_FAN, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderCylindar(shape){

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderSphere(shape){

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));
}

function render(){

    var shape = shapeWrapper.shape;

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    switch(shape.get('type')){
        case "cube":
            renderCube(shape);
            break;
        case "cone":
            renderCone(shape);
            break;
        case "cylinder":
            renderCylindar(shape);
            break;
        case "sphere":
            renderSphere(shape);
            break;
    }
}

function setupInput() {

    var canvas = document.getElementById("gl-canvas"),

        inputTranslationX = document.getElementById("input-translation-x"),
        inputTranslationY = document.getElementById("input-translation-y"),
        inputTranslationZ = document.getElementById("input-translation-z"),

        inputRotationX = document.getElementById("input-rotation-x"),
        inputRotationY = document.getElementById("input-rotation-y"),
        inputRotationZ = document.getElementById("input-rotation-z"),

        inputScaleX = document.getElementById("input-scale-x"),
        inputScaleY = document.getElementById("input-scale-y"),
        inputScaleZ = document.getElementById("input-scale-z"),

        selectMaterial = document.getElementById("select-material"),

        inputEyePositionX = document.getElementById("input-eye-position-x"),
        inputEyePositionY = document.getElementById("input-eye-position-y"),
        inputEyePositionZ = document.getElementById("input-eye-position-z"),

        selectLight = document.getElementById("select-light"),
        inputLightEnabled = document.getElementById("input-light-enabled"),

        inputLightPositionX = document.getElementById("input-light-position-x"),
        inputLightPositionY = document.getElementById("input-light-position-y"),
        inputLightPositionZ = document.getElementById("input-light-position-z"),

        inputLightAmbientR = document.getElementById("input-light-ambient-r"),
        inputLightAmbientG = document.getElementById("input-light-ambient-g"),
        inputLightAmbientB = document.getElementById("input-light-ambient-b"),

        inputLightDiffuseR = document.getElementById("input-light-diffuse-r"),
        inputLightDiffuseG = document.getElementById("input-light-diffuse-g"),
        inputLightDiffuseB = document.getElementById("input-light-diffuse-b"),

        inputLightSpecularR = document.getElementById("input-light-specular-r"),
        inputLightSpecularG = document.getElementById("input-light-specular-g"),
        inputLightSpecularB = document.getElementById("input-light-specular-b"),

        updateLightControlState = function(){

            var lightWrapper = lightArray[lightIndex],
                light = lightWrapper.light,

                lightPosition = light.get('position'),
                lightAmbient = light.get('ambient'),
                lightDiffuse = light.get('diffuse'),
                lightSpecular = light.get('specular');

            inputLightEnabled.checked = lightWrapper.isEnabled;

            //light inputs
            inputLightPositionX.value = lightPosition.x;
            inputLightPositionY.value = lightPosition.y;
            inputLightPositionZ.value = lightPosition.z;

            inputLightAmbientR.value = lightAmbient.r;
            inputLightAmbientG.value = lightAmbient.g;
            inputLightAmbientB.value = lightAmbient.b;

            inputLightDiffuseR.value = lightDiffuse.r;
            inputLightDiffuseG.value = lightDiffuse.g;
            inputLightDiffuseB.value = lightDiffuse.b;

            inputLightSpecularR.value = lightSpecular.r;
            inputLightSpecularG.value = lightSpecular.g;
            inputLightSpecularB.value = lightSpecular.b;
        },

        updateShapeControlState = function(){

            var shape = shapeWrapper.shape,
                shapeTranslation = shape.get('translation'),
                shapeRotation = shape.get('rotation'),
                shapeScale = shape.get('scale');

            selectMaterial.value = shapeWrapper.material;

            //shape inputs
            inputTranslationX.value = shapeTranslation.x;
            inputTranslationY.value = shapeTranslation.y;
            inputTranslationZ.value = shapeTranslation.z;

            inputRotationX.value = shapeRotation.x;
            inputRotationY.value = shapeRotation.y;
            inputRotationZ.value = shapeRotation.z;

            inputScaleX.value = shapeScale.x;
            inputScaleY.value = shapeScale.y;
            inputScaleZ.value = shapeScale.z;
        },

        setControlDefaults = function(){

            var i,eyePosition = eye.get('position'),
                option;

            //eye inputs
            inputEyePositionX.value = eyePosition.x;
            inputEyePositionY.value = eyePosition.y;
            inputEyePositionZ.value = eyePosition.z;

            updateShapeControlState();
            updateLightControlState();

            //initialize light select input
            for(i = 0; i < lightArray.length; i++) {

                option = document.createElement("option");
                option.value = i;
                option.text = "light " + i;

                if(i === lightIndex){
                    option.selected = true;
                }

                selectLight.add(option);
            }
        };

    setControlDefaults();

    selectLight.onchange = function(){

        lightIndex = parseInt(this.value);
        updateLightControlState();
    };

    inputLightEnabled.onchange = function(){

        lightArray[lightIndex].isEnabled = this.checked;
        updateLightStatus();
        render();
    };

    //light ambient
    inputLightAmbientR.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('ambient').r = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    inputLightAmbientG.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('ambient').g = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    inputLightAmbientB.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('ambient').b = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    //light diffuse
    inputLightDiffuseR.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('diffuse').r = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    inputLightDiffuseG.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('diffuse').g = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    inputLightDiffuseB.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('diffuse').b = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    //light diffuse
    inputLightSpecularR.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('specular').r = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    inputLightSpecularG.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('specular').g = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    inputLightSpecularB.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('specular').b = parseFloat(this.value);
        updateCurrentLightProperties();
        render();
    };

    //light position
    inputLightPositionX.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('position').x = parseFloat(this.value);
        updateLightPosition();
        render();
    };

    inputLightPositionY.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('position').y = parseFloat(this.value);
        updateLightPosition();
        render();
    };

    inputLightPositionZ.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('position').z = parseFloat(this.value);
        updateLightPosition();
        render();
    };

    //translation input callbacks
    inputTranslationX.oninput = function () {

        shapeWrapper.shape.get('translation').x = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    inputTranslationY.oninput = function () {

        shapeWrapper.shape.get('translation').y = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    inputTranslationZ.oninput = function () {

        shapeWrapper.shape.get('translation').z = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    //rotation input callbacks
    inputRotationX.oninput = function () {

        shapeWrapper.shape.get('rotation').x = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    inputRotationY.oninput = function () {

        shapeWrapper.shape.get('rotation').y = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    inputRotationZ.oninput = function () {

        shapeWrapper.shape.get('rotation').z = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    //scale input callbacks
    inputScaleX.oninput = function () {

        shapeWrapper.shape.get('scale').x = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    inputScaleY.oninput = function () {

        shapeWrapper.shape.get('scale').y = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    inputScaleZ.oninput = function () {

        shapeWrapper.shape.get('scale').z = parseFloat(this.value);
        updateShapeTransformation();
        render();
    };

    selectMaterial.onchange = function() {

        shapeWrapper.material = this.value;

        shapeWrapper.shape.get("material").set(getMaterial(this.value));
        updateAllLightProperties();
        render();
    };

    inputEyePositionX.oninput = function () {

        eye.get('position').x = parseFloat(this.value);
        updateEyeLocation();
        render();
    };

    inputEyePositionY.oninput = function () {

        eye.get('position').y = parseFloat(this.value);
        updateEyeLocation();
        render();
    };

    inputEyePositionZ.oninput = function () {

        eye.get('position').z = parseFloat(this.value);
        updateEyeLocation();
        render();
    };
}

window.onload = function(){

    try {

        init();
        setupInput();
        render();
    }
    catch (e) {
        console.log(e);
    }
};





