
var gl,
    bufferId,
    vPosition,
    vertices = [];

function init(){

    var program,
        canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.8, 0.8, 0.8, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU
    bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    //setup controls
    canvas.addEventListener("click", function(){

        var clipCoordinaes = getClipCoordinaes(event, canvas);
        //console.log(event.clientX + " : " + event.clientY);
        //console.log(clipCoordinaes[0] + " : " + clipCoordinaes[1]);

        vertices.push(clipCoordinaes);

        loadGpu();
        render();
    });
}

function loadGpu(){

    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW );
}

function getClipCoordinaes(event, canvas){

    var x = -1 + 2 * event.clientX / canvas.width,
        y = -1 + 2 * (canvas.height - event.clientY) / canvas.height;

    return vec2(x, y);
}

function render(){

    gl.clear(gl.COLOR_BUFFER_BIT);

    if(vertices.length > 0) {

        gl.drawArrays(gl.TRIANGLE_FAN, 0, vertices.length);
    }
}

window.onload = function(){

    try {

        init();
        render();
    }
    catch (e) {
        console.log(e);
    }
};