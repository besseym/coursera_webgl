
var gl,
    imageLoaded = false;

function init(){

    var program,
        vBufferId,
        tBufferId,
        aPosition,
        aTexCoord,
        uSampler,
        image,
        texture,
        canvas = document.getElementById( "gl-canvas"),
        vertexArray = [
            -0.5, -0.5,
            -0.5,  0.5,
            0.5, 0.5,
            0.5, -0.5
        ],
        texCoordArray = [
            0, 0,
            0, 1,
            1, 1,
            1, 0
        ];

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.5, 0.5, 0.5, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertexArray), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    aPosition = gl.getAttribLocation( program, "a_Position" );
    if (aPosition < 0) {
        throw "Failed to get the storage location of a_Position";
    }
    gl.vertexAttribPointer( aPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aPosition );


    tBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, tBufferId);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(texCoordArray), gl.STATIC_DRAW );

    aTexCoord = gl.getAttribLocation( program, "a_TexCoord" );
    if (aTexCoord < 0) {
        throw "Failed to get the storage location of a_TexCoord";
    }
    gl.vertexAttribPointer( aTexCoord, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aTexCoord );

    uSampler = gl.getUniformLocation( program, "u_Sampler");
    if (uSampler < 0) {
        throw "Failed to get the storage location of u_Sampler";
    }

    image = new Image();
    image.src = "/coursera_webgl/images/tile.jpg";
    image.onload = function(){

        gl.activeTexture( gl.TEXTURE0 );

        configureTexture(image);

        gl.uniform1i(uSampler, 0);

        imageLoaded = true;
    };
}

function loadingLoop(){

    if(imageLoaded){
        render();
    }
    else {
        setTimeout(loadingLoop, 500);
    }
}

function configureTexture(image) {

    var texture = gl.createTexture();
    gl.bindTexture( gl.TEXTURE_2D, texture );

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR );
    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR );

    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    //gl.generateMipmap( gl.TEXTURE_2D );

    //gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, image.width, image.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
}

function render(){

    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.TRIANGLE_FAN, 0, 4 );
}

window.onload = function(){

    try {

        init();
        loadingLoop();

    }
    catch (e) {
        console.log(e);
    }
};