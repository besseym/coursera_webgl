
//"use strict";

var gl,
    renderInterval,
    theta = 0.0,
    thetaLocation,
    renderDelay = 100,
    direction = true;

function init(){

    var canvas = document.getElementById( "gl-canvas"),
        program,
        vBuffer,
        vPosition,
        vertices = [
            vec2(  0,  1 ),
            vec2(  1,  0 ),
            vec2( -1,  0 ),
            vec2(  0, -1 )
        ];

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU
    vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    thetaLocation = gl.getUniformLocation( program, "theta" );

    //setup keyboard controls
    window.onkeydown = function( event ) {

        var key = String.fromCharCode(event.keyCode);
        switch( key ) {

            case '1':
                direction = !direction;
                break;

            case '2':
                renderDelay /= 2.0;
                setRenderInterval();
                break;

            case '3':
                renderDelay *= 2.0;
                setRenderInterval();
                break;
        }
    };
}

function render(){

    gl.clear( gl.COLOR_BUFFER_BIT );

    theta += (direction ? 0.1 : -0.1);
    gl.uniform1f(thetaLocation, theta);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}

function setRenderInterval(){

    clearInterval(renderInterval);
    renderInterval = setInterval(
        function(){
            requestAnimFrame(render);
        }, renderDelay
    );
}

window.onload = function(){

    try {

        init();
        render();

        setRenderInterval();
    }
    catch (e) {
        console.log(e);
    }
};