
var gl,

    uEyeRotationVector,

    uTranslationVector,
    uRotationVector,
    uScaleVector,

    uModelViewMatrix,
    uProjectionMatrix,

    uStaticColorFlag = 0,

    projectionMatrix,

    eye,
    shape;

function init(){

    var program,
        vBufferId,
        vPosition,
        cBufferId,
        vColor,
        canvas = document.getElementById( "gl-canvas"),
        cubeFactory = CubeFactoryCreator.getInstance(),
        coneFactory = ConeFactoryCreator.getInstance(),
        cylinderFactory = CylinderFactoryCreator.getInstance(),
        sphereFactory = SphereFactoryCreator.getInstance();

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.POLYGON_OFFSET_FILL);
    gl.polygonOffset(1.0, 2.0);

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    eye = new Eye({position: [0.0, 0.0, 1.0]});
    //shape = cubeFactory.buildCube();
    //shape = coneFactory.buildCone();
    shape = cylinderFactory.buildCylinder();
    //shape = sphereFactory.buildSphere();

    // Load the color data into the GPU
    cBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("colorArray")), gl.STATIC_DRAW );

    vColor = gl.getAttribLocation( program, "a_Color" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    //// Load the data into the GPU
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(shape.get("pointArray")), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "a_Position" );
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    uEyeRotationVector = gl.getUniformLocation(program, "a_EyeRotationVector");

    uViewMatrix = gl.getUniformLocation( program, "u_ViewMatrix" );
    uProjectionMatrix = gl.getUniformLocation( program, "u_ProjectionMatrix" );
    uPerspectiveMatrix = gl.getUniformLocation( program, "u_PerspectiveMatrix" );

    uStaticColorFlag = gl.getUniformLocation(program, "u_StaticColorFlag");

    uTranslationVector = gl.getUniformLocation(program, "u_TranslationVector");
    uRotationVector = gl.getUniformLocation(program, "u_RotationVector");
    uScaleVector = gl.getUniformLocation(program, "u_ScaleVector");

    gl.uniformMatrix4fv( uViewMatrix, false, flatten(eye.getLookAt()) );
    //gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(ortho(-1, 1, -1, 1, 5, -5)) );
    gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(perspective( 45, 1, 0.1, 10 )) );

}

function renderCube(shape){

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays( gl.TRIANGLES, 0, shape.get('pointArray').length);

    gl.uniform1i(uStaticColorFlag, 1);
    gl.drawArrays( gl.LINES, 0, shape.get('pointArray').length);
}

function renderCone(shape){

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));

    gl.uniform1i(uStaticColorFlag, 1);
    gl.drawArrays(gl.LINE_STRIP, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.LINE_STRIP, shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderCylindar(shape){

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));

    gl.uniform1i(uStaticColorFlag, 1);
    gl.drawArrays(gl.LINES, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.LINE_STRIP, shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.LINE_STRIP, shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderSphere(shape){

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shape.get('bottomOffset'), shape.get('bottomCount'));

    gl.uniform1i(uStaticColorFlag, 1);
    gl.drawArrays(gl.LINE_LOOP, 0, shape.get('bodyCount'));
    gl.drawArrays(gl.LINE_STRIP, shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.LINE_STRIP, shape.get('bottomOffset'), shape.get('bottomCount'));
}

function render(){

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.uniform3fv(uEyeRotationVector, flatten(eye.getRotationVector()) );

    gl.uniform3fv(uTranslationVector, shape.get('translation').toArray());
    gl.uniform3fv(uRotationVector, shape.get('rotation').toArray());
    gl.uniform3fv(uScaleVector, shape.get('scale').toArray());


    switch(shape.get('type')){
        case "cube":
            renderCube(shape);
            break;
        case "cone":
            renderCone(shape);
            break;
        case "cylinder":
            renderCylindar(shape);
            break;
        case "sphere":
            renderSphere(shape);
            break;
    }
}

function setupInput() {

    var canvas = document.getElementById("gl-canvas"),
        inputTranslationX = document.getElementById("input-translation-x"),
        inputTranslationY = document.getElementById("input-translation-y"),
        inputTranslationZ = document.getElementById("input-translation-z"),
        inputRotationX = document.getElementById("input-rotation-x"),
        inputRotationY = document.getElementById("input-rotation-y"),
        inputRotationZ = document.getElementById("input-rotation-z"),
        inputScaleX = document.getElementById("input-scale-x"),
        inputScaleY = document.getElementById("input-scale-y"),
        inputScaleZ = document.getElementById("input-scale-z"),
        inputEyeTheta = document.getElementById("input-eye-theta"),
        inputEyePhi = document.getElementById("input-eye-phi");

    //translation input callbacks
    inputTranslationX.oninput = function () {

        shape.get('translation').set({x: parseFloat(this.value)});
        render();
    };

    inputTranslationY.oninput = function () {

        shape.get('translation').set({y: parseFloat(this.value)});
        render();
    };

    inputTranslationZ.oninput = function () {

        shape.get('translation').set({z: parseFloat(this.value)});
        render();
    };

    //rotation input callbacks
    inputRotationX.oninput = function () {

        shape.get('rotation').set({x: parseFloat(this.value)});
        render();
    };

    inputRotationY.oninput = function () {

        shape.get('rotation').set({y: parseFloat(this.value)});
        render();
    };

    inputRotationZ.oninput = function () {

        shape.get('rotation').set({z: parseFloat(this.value)});
        render();
    };

    //scale input callbacks
    inputScaleX.oninput = function () {

        shape.get('scale').set({x: parseFloat(this.value)});
        render();
    };

    inputScaleY.oninput = function () {

        shape.get('scale').set({y: parseFloat(this.value)});
        render();
    };

    inputScaleZ.oninput = function () {

        shape.get('scale').set({z: parseFloat(this.value)});
        render();
    };

    inputEyeTheta.oninput = function () {

        eye.set({theta: parseFloat(this.value)});
        render();
    };

    inputEyePhi.oninput = function () {

        eye.set({phi: parseFloat(this.value)});
        render();
    };
}

window.onload = function(){

    try {

        init();
        setupInput();
        render();

        //setRenderInterval();
    }
    catch (e) {
        console.log(e);
    }
};