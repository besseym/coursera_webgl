
var gl,

    vBufferId,
    vPosition,
    cBufferId,
    vColor,

    uTranslationVector,
    uRotationVector,
    uScaleVector,

    uViewMatrix,
    uProjectionMatrix,

    uStaticColorFlag = -1,

    shapeArray = [],
    shapeIndex = 0,

    coneFactory = ConeFactoryCreator.getInstance(),
    cubeFactory = CubeFactoryCreator.getInstance(),
    cylinderFactory = CylinderFactoryCreator.getInstance(),
    sphereFactory = SphereFactoryCreator.getInstance(),

    eye = new Eye({position: {x: 0.0, y: 0.0, z: 3.0}}),

    bufferSize = 512 * 512 * 8;

function init(){

    var program,
        canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.POLYGON_OFFSET_FILL);
    gl.polygonOffset(1.0, 2.0);

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the color data into the GPU
    cBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, bufferSize, gl.DYNAMIC_DRAW );
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(shapeArray[shapeIndex].colors), gl.STATIC_DRAW );

    vColor = gl.getAttribLocation( program, "a_Color" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    //// Load the data into the GPU
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, bufferSize, gl.DYNAMIC_DRAW );
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(shapeArray[shapeIndex].points), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "a_Position" );
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    gl.lineWidth(2);

    uTranslationVector = gl.getUniformLocation(program, "u_TranslationVector");
    uRotationVector = gl.getUniformLocation(program, "u_RotationVector");
    uScaleVector = gl.getUniformLocation(program, "u_ScaleVector");

    uStaticColorFlag = gl.getUniformLocation(program, "u_StaticColorFlag");

    uViewMatrix = gl.getUniformLocation( program, "u_ViewMatrix" );
    gl.uniformMatrix4fv( uViewMatrix, false, flatten(eye.getLookAt()) );

    uProjectionMatrix = gl.getUniformLocation( program, "u_ProjectionMatrix" );
    //gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(ortho(-1, 1, -1, 1, 5, -5)) );
    gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(perspective( 45, canvas.width/canvas.height, 0.1, 10 )) );
}

function renderCube(i, shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLES, shapeWrapper.vOffset, shape.get('pointArray').length);

    if(i == shapeIndex){
        gl.uniform1i(uStaticColorFlag, 1);
    }
    else {
        gl.uniform1i(uStaticColorFlag, 0);
    }

    gl.drawArrays(gl.LINES, shapeWrapper.vOffset, shape.get('pointArray').length);
}

function renderCone(i, shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.vOffset, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.vOffset + shape.get('bottomOffset'), shape.get('bottomCount'));

    if(i == shapeIndex){
        gl.uniform1i(uStaticColorFlag, 1);
    }
    else {
        gl.uniform1i(uStaticColorFlag, 0);
    }

    gl.drawArrays(gl.LINE_STRIP, shapeWrapper.vOffset, shape.get('bodyCount'));
    gl.drawArrays(gl.LINE_STRIP, shapeWrapper.vOffset + shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderCylindar(i, shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLE_STRIP, shapeWrapper.vOffset, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.vOffset + shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.vOffset + shape.get('bottomOffset'), shape.get('bottomCount'));

    if(i == shapeIndex){
        gl.uniform1i(uStaticColorFlag, 1);
    }
    else {
        gl.uniform1i(uStaticColorFlag, 0);
    }

    gl.drawArrays(gl.LINES, shapeWrapper.vOffset, shape.get('bodyCount'));
    gl.drawArrays(gl.LINE_STRIP, shapeWrapper.vOffset + shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.LINE_STRIP, shapeWrapper.vOffset + shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderSphere(i, shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.uniform1i(uStaticColorFlag, -1);
    gl.drawArrays(gl.TRIANGLE_STRIP, shapeWrapper.vOffset, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.vOffset + shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.vOffset + shape.get('bottomOffset'), shape.get('bottomCount'));

    if(i == shapeIndex){
        gl.uniform1i(uStaticColorFlag, 1);
    }
    else {
        gl.uniform1i(uStaticColorFlag, 0);
    }

    gl.drawArrays(gl.LINE_LOOP, shapeWrapper.vOffset, shape.get('bodyCount'));
    gl.drawArrays(gl.LINE_STRIP, shapeWrapper.vOffset + shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.LINE_STRIP, shapeWrapper.vOffset + shape.get('bottomOffset'), shape.get('bottomCount'));
}

function render(){

    var i = 0,
        shapeWrapper;

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if(shapeArray.length > 0){

        for(i = 0; i < shapeArray.length; i++){

            shapeWrapper = shapeArray[i];

            gl.uniform3fv(uTranslationVector, glUtil.pointToArray(shapeWrapper.shape.get('translation')));
            gl.uniform3fv(uRotationVector, glUtil.pointToArray(shapeWrapper.shape.get('rotation')));
            gl.uniform3fv(uScaleVector, glUtil.pointToArray(shapeWrapper.shape.get('scale')));

            switch(shapeWrapper.shape.get('type')){
                case "cube":
                    renderCube(i, shapeWrapper);
                    break;
                case "cone":
                    renderCone(i, shapeWrapper);
                    break;
                case "cylinder":
                    renderCylindar(i, shapeWrapper);
                    break;
                case "sphere":
                    renderSphere(i, shapeWrapper);
                    break;
            }
        }
    }
}

function setupInput() {

    var canvas = document.getElementById("gl-canvas"),

        buttonCreate = document.getElementById("button-create"),
        inputShapeArray = document.getElementsByName("input-shape"),

        shapeValue = "cube",
        inputShape = document.getElementById("input-shape"),
        inputColor = document.getElementById("input-color"),

        selectShape = document.getElementById("select-shape"),

        inputTranslationX = document.getElementById("input-translation-x"),
        inputTranslationY = document.getElementById("input-translation-y"),
        inputTranslationZ = document.getElementById("input-translation-z"),

        inputRotationX = document.getElementById("input-rotation-x"),
        inputRotationY = document.getElementById("input-rotation-y"),
        inputRotationZ = document.getElementById("input-rotation-z"),

        inputScaleX = document.getElementById("input-scale-x"),
        inputScaleY = document.getElementById("input-scale-y"),
        inputScaleZ = document.getElementById("input-scale-z"),

        enableModificationControls = function(){

            selectShape.disabled = false;

            inputColor.disabled = false;

            inputTranslationX.disabled = false;
            inputTranslationY.disabled = false;
            inputTranslationZ.disabled = false;

            inputRotationX.disabled = false;
            inputRotationY.disabled = false;
            inputRotationZ.disabled = false;

            inputScaleX.disabled = false;
            inputScaleY.disabled = false;
            inputScaleZ.disabled = false;
        },
        inputShapeOnChange = function () {

            shapeValue = this.value;
        },
        setControlDefaults = function(){

            inputColor.value = "#A9A9A9";

            inputTranslationX.value = 0;
            inputTranslationY.value = 0;
            inputTranslationZ.value = 0;

            inputRotationX.value = 0;
            inputRotationY.value = 0;
            inputRotationZ.value = 0;

            inputScaleX.value = 1;
            inputScaleY.value = 1;
            inputScaleZ.value = 1;
        },
        reloadColorBuffer = function(shapeWrapper) {

            gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
            gl.bufferSubData(gl.ARRAY_BUFFER, shapeWrapper.cByteOffset, flatten(shapeWrapper.shape.get('colorArray')));
        },
        addShape = function(shape){

            var i = 0,
                vByteOffset = 0,
                cByteOffset = 0,
                vOffset = 0,
                lastShape,
                option = document.createElement("option");

            if(shapeArray.length === 0){
                enableModificationControls();
            }

            if(shapeArray.length > 0){

                lastShape = shapeArray[shapeArray.length - 1];

                vByteOffset = lastShape.vByteOffset + (lastShape.shape.get('pointArray').length * sizeof.vec3);
                cByteOffset = lastShape.cByteOffset + (lastShape.shape.get('colorArray').length * sizeof.vec4);

                vOffset = lastShape.vOffset + lastShape.shape.get('pointArray').length;
            }

            shapeArray.push({
                shape: shape,
                vByteOffset: vByteOffset,
                cByteOffset: cByteOffset,
                vOffset: vOffset,
                isActive: true
            });

            shapeIndex = shapeArray.length - 1;

            gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
            gl.bufferSubData(gl.ARRAY_BUFFER, cByteOffset, flatten(shape.get('colorArray')));

            gl.bindBuffer(gl.ARRAY_BUFFER, vBufferId);
            gl.bufferSubData(gl.ARRAY_BUFFER, vByteOffset, flatten(shape.get('pointArray')));

            //add shape dropdown option
            option.value = shapeIndex;
            option.text = shapeArray[shapeIndex].shape.get('type') + shapeIndex;

            selectShape.add(option);
            selectShape.value = shapeIndex;

            //set control defaults
            setControlDefaults();
        };


    //set control defaults
    setControlDefaults();

    for (i = 0; i < inputShapeArray.length; i++) {

        inputShapeArray[i].onchange = inputShapeOnChange;
        if(inputShapeArray[i].checked){
            shapeValue = inputShapeArray[i].value;
        }
    }

    inputColor.onchange = function(){

        var color = new Color(),
            shapeWrapper = shapeArray[shapeIndex];

        color.convertHex(this.value);

        shapeWrapper.shape.setColor(color.toArray());
        reloadColorBuffer(shapeWrapper);

        //cubeFactory.set({color: color.toArray()});
        //coneFactory.set({color: color.toArray()});
        //cylinderFactory.set({color: color.toArray()});
        //sphereFactory.set({color: color.toArray()});

        render();
    };

    buttonCreate.onclick = function (event) {

        var shape;

        switch(shapeValue){
            case 'cube':
                shape = cubeFactory.buildCube();
                break;
            case 'cone':
                shape = coneFactory.buildCone();
                break;
            case 'cylinder':
                shape = cylinderFactory.buildCylinder();
                break;
            case 'sphere':
                shape = sphereFactory.buildSphere();
                break;
            default:
                break;
        }

        event.preventDefault();

        if(shape !== undefined) {

            addShape(shape);
            render();
        }

    };

    selectShape.onchange = function(){

        var v = parseInt(this.value),
            shape;

        if(v >= 0){

            shapeIndex = v;
            shape = shapeArray[shapeIndex].shape;

            inputTranslationX.value = shape.get('translation').x;
            inputTranslationY.value = shape.get('translation').y;
            inputTranslationZ.value = shape.get('translation').z;

            inputRotationX.value = shape.get('rotation').x;
            inputRotationY.value = shape.get('rotation').y;
            inputRotationZ.value = shape.get('rotation').z;

            inputScaleX.value = shape.get('scale').x;
            inputScaleY.value = shape.get('scale').y;
            inputScaleZ.value = shape.get('scale').z;
        }

        render();
    };

    //translation input callbacks
    inputTranslationX.oninput = function () {

        shapeArray[shapeIndex].shape.get('translation').x = parseFloat(this.value);
        render();
    };

    inputTranslationY.oninput = function () {

        shapeArray[shapeIndex].shape.get('translation').y = parseFloat(this.value);
        render();
    };

    inputTranslationZ.oninput = function () {

        shapeArray[shapeIndex].shape.get('translation').z = parseFloat(this.value);
        render();
    };

    //rotation input callbacks
    inputRotationX.oninput = function () {

        shapeArray[shapeIndex].shape.get('rotation').x = parseFloat(this.value);
        render();
    };

    inputRotationY.oninput = function () {

        shapeArray[shapeIndex].shape.get('rotation').y = parseFloat(this.value);
        render();
    };

    inputRotationZ.oninput = function () {

        shapeArray[shapeIndex].shape.get('rotation').z = parseFloat(this.value);
        render();
    };

    //scale input callbacks
    inputScaleX.oninput = function () {

        shapeArray[shapeIndex].shape.get('scale').x = parseFloat(this.value);
        render();
    };

    inputScaleY.oninput = function () {

        shapeArray[shapeIndex].shape.get('scale').y = parseFloat(this.value);
        render();
    };

    inputScaleZ.oninput = function () {

        shapeArray[shapeIndex].shape.get('scale').z = parseFloat(this.value);
        render();
    };
}

window.onload = function(){

    try {

        init();
        setupInput();
        render();

        //setRenderInterval();
    }
    catch (e) {
        console.log(e);
    }
};

