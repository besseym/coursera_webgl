
var gl;

function init(){

    var program,
        bufferId,
        vPosition,
        canvas = document.getElementById( "gl-canvas"),
        vertices = [
            -0.5, -0.5,
            -0.5,  0.5,
            0.5, 0.5,
            0.5, -0.5];

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU
    bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    return gl;
}

function render(){

    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.TRIANGLE_FAN, 0, 4 );
}

window.onload = function(){

    try {

        init();
        render();
    }
    catch (e) {
        console.log(e);
    }
};