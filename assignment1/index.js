
var gl,
    program,
    bufferId,
    vPosition,
    points = [],
    renderStyle = 'line',
    theta = 0.0,
    subdivideCount = 0,
    shapeBuilder,
    triangleBuilder,
    squareBuilder;

triangleBuilder = function (){

    //equilateral triangle
    var h = 0.866,
        hs = h / 3,
        vertices = [
            vec2( -0.5, -hs ),
            vec2(  0.0,  hs * 2 ),
            vec2(  0.5, -hs )
        ],
        triangleArray = [];

    triangleArray.push(vertices);

    return triangleArray;
};

squareBuilder = function (){

    //equilateral triangle
    var vertices1 = [
            vec2( -0.5, -0.5 ),
            vec2( -0.5,  0.5 ),
            vec2(  0.5, 0.5 )
        ],
        vertices2 = [
            vec2( -0.5, -0.5),
            vec2(  0.5, 0.5 ),
            vec2(  0.5, -0.5 )
        ],
        triangleArray = [];

    triangleArray.push(vertices1);
    triangleArray.push(vertices2);

    return triangleArray;
};

function init(){

    var canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.enableVertexAttribArray( vPosition );

    bufferId = gl.createBuffer();

    shapeBuilder = triangleBuilder;
    calculatePoints();

    loadGpu();
}

function loadGpu(){

    // Load the data into the GPU

    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );
}

function render() {

    var i = 0, thetaLoc;

    gl.clear(gl.COLOR_BUFFER_BIT);

    thetaLoc = gl.getUniformLocation(program, "theta");
    gl.uniform1f(thetaLoc, theta);

    vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);

    //gl.drawArrays(gl.TRIANGLES, 0, points.length);

    if(renderStyle === 'line') {

        //draw wireframe
        for (i = 0; i < points.length; i = i + 3) {
            gl.drawArrays(gl.LINE_LOOP, i, 3);
        }
    }
    else if(renderStyle === 'fill') {

        gl.drawArrays(gl.TRIANGLES, 0, points.length);
    }
}

function calculatePoints(){

    var i, vertices,
        triangleArray = shapeBuilder();

    points = [];

    for(i = 0; i < triangleArray.length; i++) {

        vertices = triangleArray[i];
        divideTriangle(vertices[0], vertices[1], vertices[2], subdivideCount);
    }
}

function divideTriangle( a, b, c, count )
{

    // check for end of recursion

    if ( count === 0 ) {
        triangle( a, b, c );
    }
    else {

        //bisect the sides

        var ab = mix( a, b, 0.5 );
        var ac = mix( a, c, 0.5 );
        var bc = mix( b, c, 0.5 );

        --count;

        // three new triangles

        divideTriangle( a, ab, ac, count );
        divideTriangle( c, ac, bc, count );
        divideTriangle( b, bc, ab, count );
        divideTriangle( ac, ab, bc, count );
    }
}

function triangle( a, b, c )
{
    points.push( a, b, c );
}

function setupInput() {

    var i = 0,
        inputDivisions = document.getElementById("input-divisions"),
        outputDivisions = document.getElementById("output-divisions"),
        inputTwist = document.getElementById("input-twist"),
        outputTwist = document.getElementById("output-twist"),
        inputShapeArray = document.getElementsByName("input-shape"),
        inputShapeOnChange = function () {

            switch(this.value){
                case 'triangle':
                    shapeBuilder = triangleBuilder;
                    break;
                case 'square':
                    shapeBuilder = squareBuilder;
                    break;
                default:
                    shapeBuilder = triangleBuilder;
                    break;
            }
            //console.log(this.value);

            calculatePoints();
            loadGpu();
            render();
        },
        inputRenderArray = document.getElementsByName("input-render"),
        inputRenderOnChange = function () {

            renderStyle = this.value;
            render();
        };

    inputDivisions.onchange = function () {

        subdivideCount = parseInt(this.value);

        outputDivisions.value = subdivideCount;

        calculatePoints();
        loadGpu();
        render();
    };

    inputTwist.onchange = function () {

        theta = parseFloat(this.value);

        outputTwist.value = theta;

        //refreshProgramVariables();
        render();
    };

    for (i = 0; i < inputShapeArray.length; i++) {

        inputShapeArray[i].onchange = inputShapeOnChange;
    }

    for (i = 0; i < inputRenderArray.length; i++) {

        inputRenderArray[i].onchange = inputRenderOnChange;
    }
}

window.onload = function(){

    try {

        init();
        setupInput();

        render();
    }
    catch (e) {
        console.log(e);
    }
};