
var gl,
    vertices1 = [-1, 1, 0, 0, 1, 1],
    vertices2 = [-1, -1, 0, 0, 1, -1];

function init(){

    var program,
        bufferId,
        vPosition,
        canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU
    bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, 8 * 512 * 512, gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    return gl;
}

function render(){

    gl.clear( gl.COLOR_BUFFER_BIT );

    //gl.bufferSubData(gl.ARRAY_BUFFER, offset * 4, flatten(vertices));
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(vertices1));
    gl.drawArrays( gl.TRIANGLES, 0, 3 );
    gl.bufferSubData(gl.ARRAY_BUFFER, 8*3, flatten(vertices2));
    gl.drawArrays( gl.TRIANGLES, 3, 6 );


}

window.onload = function(){

    try {
        init();
        render();
    }
    catch (e) {
        console.log(e);
    }
};