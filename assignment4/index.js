
var gl,

    vBufferId,
    aPosition,

    nBufferId,
    aNormal,

    uTranslationVector,
    uRotationVector,
    uScaleVector,

    uViewMatrix,
    uProjectionMatrix,

    uStaticColorFlag = -1,

    shapeArray = [],
    shapeIndex = 0,

    uLightColor,

    uLightIsEnabled0,
    uLightPosition0,
    uAmbient0,
    uDiffuse0,
    uSpecular0,
    uShininess0,

    uLightIsEnabled1,
    uLightPosition1,
    uAmbient1,
    uDiffuse1,
    uSpecular1,
    uShininess1,

    lightArray = [],
    lightIndex = 0,

    lightShapeFactory = SphereFactoryCreator.getInstance({radius: 0.05}),

    coneFactory = ConeFactoryCreator.getInstance(),
    cubeFactory = CubeFactoryCreator.getInstance(),
    cylinderFactory = CylinderFactoryCreator.getInstance(),
    sphereFactory = SphereFactoryCreator.getInstance(),

    eye = new Eye({position: {x: 1.0, y: 2.0, z: 3.0}}),

    bufferSize = 512 * 512 * 8;

function init(){

    var program,
        canvas = document.getElementById( "gl-canvas");

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) {
        throw "WebGL isn't available!";
    }

    // Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.1, 0.1, 0.1, 1.0 );

    gl.enable(gl.DEPTH_TEST);

    // Load shaders and initialize attribute buffers
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the normal data into the GPU
    nBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, nBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, bufferSize, gl.STATIC_DRAW );

    aNormal = gl.getAttribLocation( program, "a_Normal" );
    gl.vertexAttribPointer( aNormal, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( aNormal );

    //// Load the data into the GPU
    vBufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBufferId );
    gl.bufferData( gl.ARRAY_BUFFER, bufferSize, gl.DYNAMIC_DRAW );
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(shapeArray[shapeIndex].points), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "a_Position" );
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    gl.lineWidth(2);

    uTranslationVector = gl.getUniformLocation(program, "u_TranslationVector");
    uRotationVector = gl.getUniformLocation(program, "u_RotationVector");
    uScaleVector = gl.getUniformLocation(program, "u_ScaleVector");

    uStaticColorFlag = gl.getUniformLocation(program, "u_StaticColorFlag");

    uLightColor = gl.getUniformLocation(program, "u_LightColor");

    //light 0
    uLightIsEnabled0 = gl.getUniformLocation(program, "u_LightIsEnabled0");
    uLightPosition0 = gl.getUniformLocation(program, "u_LightPosition0");

    uAmbient0 = gl.getUniformLocation(program, "u_Ambient0");
    uDiffuse0 = gl.getUniformLocation(program, "u_Diffuse0");
    uSpecular0 = gl.getUniformLocation(program, "u_Specular0");
    uShininess0 = gl.getUniformLocation(program, "u_Shininess0");

    //light 1
    uLightIsEnabled1 = gl.getUniformLocation(program, "u_LightIsEnabled1");
    uLightPosition1 = gl.getUniformLocation(program, "u_LightPosition1");

    uAmbient1 = gl.getUniformLocation(program, "u_Ambient1");
    uDiffuse1 = gl.getUniformLocation(program, "u_Diffuse1");
    uSpecular1 = gl.getUniformLocation(program, "u_Specular1");
    uShininess1 = gl.getUniformLocation(program, "u_Shininess1");


    uViewMatrix = gl.getUniformLocation( program, "u_ViewMatrix" );
    uProjectionMatrix = gl.getUniformLocation( program, "u_ProjectionMatrix" );

    updateEyeLocation();
    //gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(ortho(-1, 1, -1, 1, 5, -5)) );
    gl.uniformMatrix4fv( uProjectionMatrix, false, flatten(perspective( 45, canvas.width/canvas.height, 0.1, 10 )) );


    setupScene();
}

function updateEyeLocation(){

    gl.uniformMatrix4fv( uViewMatrix, false, flatten(eye.getLookAt()) );
}

function setupScene(){

    var i = 0,
        shape;

    addLights();

    shape = cubeFactory.buildShape();
    shape.set({translation: {x: 0.5, y: 0.0, z: 0.0}});
    addShape(shape, "ruby");

    shape = coneFactory.buildShape();
    shape.set({translation: {x: -0.5, y: 0.0, z: 0.0}});
    addShape(shape, "jade");

    shape = sphereFactory.buildShape();
    shape.set({translation: {x: 0.0, y: 0.0, z: -0.5}});
    addShape(shape, "pearl");

    shape = cylinderFactory.buildShape();
    shape.set({translation: {x: 0.0, y: 0.0, z: 0.5}});
    addShape(shape, "brass");

    for(i = 0; i < lightArray.length; i++) {
        updateLightStatus(i);
        updateLightPosition(i);
    }
}

function addShape(shape, materialValue){

    var i = 0,
        vByteOffset = 0,
        nByteOffset = 0,
        offset = 0,
        lastShape,
        lastLight;

    if(shapeArray.length > 0){

        lastShape = shapeArray[shapeArray.length - 1];

        vByteOffset = lastShape.vByteOffset + (lastShape.shape.get('pointArray').length * sizeof.vec3);
        nByteOffset = lastShape.nByteOffset + (lastShape.shape.get('normalArray').length * sizeof.vec3);

        offset = lastShape.offset + lastShape.shape.get('pointArray').length;
    }
    else if(lightArray.length > 0) {

        lastLight = lightArray[lightArray.length - 1];

        vByteOffset = lastLight.vByteOffset + (lastLight.shape.get('pointArray').length * sizeof.vec3);
        nByteOffset = lastLight.nByteOffset + (lastLight.shape.get('normalArray').length * sizeof.vec3);

        offset = lastLight.offset + lastLight.shape.get('pointArray').length;
    }

    shape.get("material").set(getMaterial(materialValue));

    shapeArray.push({
        shape: shape,
        vByteOffset: vByteOffset,
        nByteOffset: nByteOffset,
        offset: offset,
        material: materialValue,
        isActive: true
    });

    shapeIndex = shapeArray.length - 1;

    gl.bindBuffer(gl.ARRAY_BUFFER, nBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, nByteOffset, flatten(shape.get('normalArray')));

    gl.bindBuffer(gl.ARRAY_BUFFER, vBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, vByteOffset, flatten(shape.get('pointArray')));
}

function addLight(light){

    var i = 0,
        shape,
        vByteOffset = 0,
        nByteOffset = 0,
        offset = 0,
        lastLight;

    if(lightArray.length > 0){

        lastLight = lightArray[lightArray.length - 1];

        vByteOffset = lastLight.vByteOffset + (lastLight.shape.get('pointArray').length * sizeof.vec3);
        nByteOffset = lastLight.nByteOffset + (lastLight.shape.get('normalArray').length * sizeof.vec3);

        offset = lastLight.offset + lastLight.shape.get('pointArray').length;
    }

    shape = lightShapeFactory.buildShape();

    lightArray.push({
        shape: shape,
        light: light,
        vByteOffset: vByteOffset,
        nByteOffset: nByteOffset,
        offset: offset,
        isEnabled: true
    });

    lightIndex = lightArray.length - 1;

    gl.bindBuffer(gl.ARRAY_BUFFER, nBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, nByteOffset, flatten(shape.get('normalArray')));

    gl.bindBuffer(gl.ARRAY_BUFFER, vBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, vByteOffset, flatten(shape.get('pointArray')));
}

function addLights(){

    addLight(new Light({
        position: {x: 0.0, y: 0.75, z: 0.25},
        ambient: {r: 0.5, g: 0.5, b: 0.5, a: 1.0},
        diffuse: {r: 0.5, g: 0.5, b: 0.5, a: 1.0},
        specular: {r: 0.2, g: 0.2, b: 0.2, a: 1.0}
    }));

    addLight(new Light({
        position: {x: 0.0, y: -0.75, z: -0.25},
        ambient: {r: 0.5, g: 0.5, b: 0.5, a: 1.0},
        diffuse: {r: 0.5, g: 0.5, b: 0.5, a: 1.0},
        specular: {r: 0.2, g: 0.2, b: 0.2, a: 1.0}
    }));
}

function updateLightStatus(index){

    var isEnabled = lightArray[index].isEnabled;

    if(index === 0) {
        gl.uniform1i(uLightIsEnabled0, isEnabled);
    }
    else if(index === 1) {
        gl.uniform1i(uLightIsEnabled1, isEnabled);
    }
}

function updateLightColor(light){

    var lightAmbient = light.get('ambient'),
        lightDiffuse = light.get('diffuse'),
        lightSpecular = light.get('specular'),
        r = lightAmbient.r + lightDiffuse.r + lightSpecular.r,
        g = lightAmbient.g + lightDiffuse.g + lightSpecular.g,
        b = lightAmbient.b + lightDiffuse.b + lightSpecular.b;

    gl.uniform3fv(uLightColor, flatten([r, g, b]));
}

function updateLightPosition(index){

    var lightWrapper = lightArray[index],
        light = lightWrapper.light;

    if(index === 0) {
        gl.uniform3fv(uLightPosition0, flatten(glUtil.pointToArray(light.get("position"))));
    }
    else if(index === 1) {
        gl.uniform3fv(uLightPosition1, flatten(glUtil.pointToArray(light.get("position"))));
    }

    lightWrapper.shape.set({translation: light.get("position")});
}

function updateShapeLightProperties(shape){

    var i = 0;

    for(i = 0; i < lightArray.length; i++){
        updateLightProperties(i, shape);
    }
}

function updateLightProperties(index, shape){

    var lightWrapper = lightArray[index],
        light = lightWrapper.light,
        materialLight = shape.get("material").calculateLight(light);

    if(index === 0) {

        gl.uniform4fv(uAmbient0, flatten(materialLight.ambient));
        gl.uniform4fv(uDiffuse0, flatten(materialLight.diffuse));
        gl.uniform4fv(uSpecular0, flatten(materialLight.specular));
        gl.uniform1f(uShininess0, shape.get("material").get("shininess"));
    }
    else if(index === 1) {

        gl.uniform4fv(uAmbient1, flatten(materialLight.ambient));
        gl.uniform4fv(uDiffuse1, flatten(materialLight.diffuse));
        gl.uniform4fv(uSpecular1, flatten(materialLight.specular));
        gl.uniform1f(uShininess1, shape.get("material").get("shininess"));
    }
}

function renderCube(shapeWrapper){

    var shape = shapeWrapper.shape;
    gl.drawArrays(gl.TRIANGLES, shapeWrapper.offset, shape.get('pointArray').length);
}

function renderCone(shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.offset, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.offset + shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderCylindar(shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.drawArrays(gl.TRIANGLE_STRIP, shapeWrapper.offset, shape.get('bodyCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.offset + shape.get('topOffset'), shape.get('topCount'));
    gl.drawArrays(gl.TRIANGLE_FAN, shapeWrapper.offset + shape.get('bottomOffset'), shape.get('bottomCount'));
}

function renderSphere(shapeWrapper){

    var shape = shapeWrapper.shape;

    gl.drawArrays(gl.TRIANGLE_STRIP, shapeWrapper.offset, shape.get('bodyCount'));
}

function render(){

    var i = 0,
        lightWrapper,
        shapeWrapper;

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if(lightArray.length > 0){

        gl.uniform1i(uStaticColorFlag, 1);

        for(i = 0; i < lightArray.length; i++){

            lightWrapper = lightArray[i];

            if(lightWrapper.isEnabled) {

                updateLightColor(lightWrapper.light);

                gl.uniform3fv(uTranslationVector, glUtil.pointToArray(lightWrapper.shape.get('translation')));
                gl.uniform3fv(uRotationVector, glUtil.pointToArray(lightWrapper.shape.get('rotation')));
                gl.uniform3fv(uScaleVector, glUtil.pointToArray(lightWrapper.shape.get('scale')));

                renderSphere(lightWrapper);
            }
        }
    }

    if(shapeArray.length > 0){

        gl.uniform1i(uStaticColorFlag, -1);

        for(i = 0; i < shapeArray.length; i++){

            shapeWrapper = shapeArray[i];

            gl.uniform3fv(uTranslationVector, glUtil.pointToArray(shapeWrapper.shape.get('translation')));
            gl.uniform3fv(uRotationVector, glUtil.pointToArray(shapeWrapper.shape.get('rotation')));
            gl.uniform3fv(uScaleVector, glUtil.pointToArray(shapeWrapper.shape.get('scale')));

            updateShapeLightProperties(shapeWrapper.shape);

            switch(shapeWrapper.shape.get('type')){
                case "cube":
                    renderCube(shapeWrapper);
                    break;
                case "cone":
                    renderCone(shapeWrapper);
                    break;
                case "cylinder":
                    renderCylindar(shapeWrapper);
                    break;
                case "sphere":
                    renderSphere(shapeWrapper);
                    break;
            }
        }
    }
}

function setupInput() {

    var canvas = document.getElementById("gl-canvas"),

        selectShape = document.getElementById("select-shape"),

        inputShapeTranslationX = document.getElementById("input-shape-translation-x"),
        inputShapeTranslationY = document.getElementById("input-shape-translation-y"),
        inputShapeTranslationZ = document.getElementById("input-shape-translation-z"),

        inputShapeRotationX = document.getElementById("input-shape-rotation-x"),
        inputShapeRotationY = document.getElementById("input-shape-rotation-y"),
        inputShapeRotationZ = document.getElementById("input-shape-rotation-z"),

        selectShapeMaterial = document.getElementById("select-shape-material"),

        selectLight = document.getElementById("select-light"),

        inputLightEnabled = document.getElementById("input-light-enabled"),

        inputLightPositionX = document.getElementById("input-light-position-x"),
        inputLightPositionY = document.getElementById("input-light-position-y"),
        inputLightPositionZ = document.getElementById("input-light-position-z"),

        inputLightAmbientR = document.getElementById("input-light-ambient-r"),
        inputLightAmbientG = document.getElementById("input-light-ambient-g"),
        inputLightAmbientB = document.getElementById("input-light-ambient-b"),

        inputLightDiffuseR = document.getElementById("input-light-diffuse-r"),
        inputLightDiffuseG = document.getElementById("input-light-diffuse-g"),
        inputLightDiffuseB = document.getElementById("input-light-diffuse-b"),

        inputLightSpecularR = document.getElementById("input-light-specular-r"),
        inputLightSpecularG = document.getElementById("input-light-specular-g"),
        inputLightSpecularB = document.getElementById("input-light-specular-b"),

        inputEyePositionX = document.getElementById("input-eye-position-x"),
        inputEyePositionY = document.getElementById("input-eye-position-y"),
        inputEyePositionZ = document.getElementById("input-eye-position-z"),

        updateShapeControls = function(){

            var shapeWrapper,
                shape;

            if(shapeArray.length <= 0){
                return;
            }

            shapeWrapper = shapeArray[shapeIndex];
            shape = shapeWrapper.shape;

            selectShapeMaterial.value = shapeWrapper.material;

            inputShapeTranslationX.value = shape.get('translation').x;
            inputShapeTranslationY.value = shape.get('translation').y;
            inputShapeTranslationZ.value = shape.get('translation').z;

            inputShapeRotationX.value = shape.get('rotation').x;
            inputShapeRotationY.value = shape.get('rotation').y;
            inputShapeRotationZ.value = shape.get('rotation').z;
        },
        updateLightControls = function(){

            var lightWrapper = lightArray[lightIndex],
                light = lightWrapper.light,

                lightPosition = light.get('position'),
                lightAmbient = light.get('ambient'),
                lightDiffuse = light.get('diffuse'),
                lightSpecular = light.get('specular');

            inputLightEnabled.checked = lightWrapper.isEnabled;

            //light inputs
            inputLightPositionX.value = lightPosition.x;
            inputLightPositionY.value = lightPosition.y;
            inputLightPositionZ.value = lightPosition.z;

            inputLightAmbientR.value = lightAmbient.r;
            inputLightAmbientG.value = lightAmbient.g;
            inputLightAmbientB.value = lightAmbient.b;

            inputLightDiffuseR.value = lightDiffuse.r;
            inputLightDiffuseG.value = lightDiffuse.g;
            inputLightDiffuseB.value = lightDiffuse.b;

            inputLightSpecularR.value = lightSpecular.r;
            inputLightSpecularG.value = lightSpecular.g;
            inputLightSpecularB.value = lightSpecular.b;

        },
        setLightControlState = function(isEnabled){

            //light inputs
            inputLightPositionX.disabled = !isEnabled;
            inputLightPositionY.disabled = !isEnabled;
            inputLightPositionZ.disabled = !isEnabled;

            inputLightAmbientR.disabled = !isEnabled;
            inputLightAmbientG.disabled = !isEnabled;
            inputLightAmbientB.disabled = !isEnabled;

            inputLightDiffuseR.disabled = !isEnabled;
            inputLightDiffuseG.disabled = !isEnabled;
            inputLightDiffuseB.disabled = !isEnabled;

            inputLightSpecularR.disabled = !isEnabled;
            inputLightSpecularG.disabled = !isEnabled;
            inputLightSpecularB.disabled = !isEnabled;
        },
        initializeControls = function(){

            var i,
                shape,
                option;

            //initialize shape select input
            for(i = 0; i < shapeArray.length; i++) {

                shape = shapeArray[i].shape;

                option = document.createElement("option");
                option.value = i;
                option.text = shape.get('type');

                if(i === shapeIndex){
                    option.selected = true;
                }

                selectShape.add(option);
            }

            //initialize light select input
            for(i = 0; i < lightArray.length; i++) {

                option = document.createElement("option");
                option.value = i;
                option.text = "light " + i;

                if(i === lightIndex){
                    option.selected = true;
                }

                selectLight.add(option);
            }

            updateShapeControls();
            updateLightControls();
        };


    //set control defaults
    initializeControls();

    selectShape.onchange = function(){

        shapeIndex = parseInt(this.value);
        updateShapeControls();
    };

    //translation transformation input
    inputShapeTranslationX.oninput = function () {

        shapeArray[shapeIndex].shape.get('translation').x = parseFloat(this.value);
        render();
    };

    inputShapeTranslationY.oninput = function () {

        shapeArray[shapeIndex].shape.get('translation').y = parseFloat(this.value);
        render();
    };

    inputShapeTranslationZ.oninput = function () {

        shapeArray[shapeIndex].shape.get('translation').z = parseFloat(this.value);
        render();
    };

    //rotation input callbacks
    inputShapeRotationX.oninput = function () {

        shapeArray[shapeIndex].shape.get('rotation').x = parseFloat(this.value);
        render();
    };

    inputShapeRotationY.oninput = function () {

        shapeArray[shapeIndex].shape.get('rotation').y = parseFloat(this.value);
        render();
    };

    inputShapeRotationZ.oninput = function () {

        shapeArray[shapeIndex].shape.get('rotation').z = parseFloat(this.value);
        render();
    };

    selectShapeMaterial.onchange = function() {

        var shapeWrapper = shapeArray[shapeIndex];

        shapeWrapper.material = this.value;
        shapeWrapper.shape.get("material").set(getMaterial(this.value));

        render();
    };

    selectLight.onchange = function(){

        lightIndex = parseInt(this.value);
        updateLightControls();
        setLightControlState(lightArray[lightIndex].isEnabled);
    };

    //light enable input
    inputLightEnabled.onchange = function(){

        lightArray[lightIndex].isEnabled = this.checked;
        setLightControlState(this.checked);
        updateLightStatus(lightIndex);
        render();
    };

    //light position input
    inputLightPositionX.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('position').x = parseFloat(this.value);
        updateLightPosition(lightIndex);
        render();
    };

    inputLightPositionY.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('position').y = parseFloat(this.value);
        updateLightPosition(lightIndex);
        render();
    };

    inputLightPositionZ.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('position').z = parseFloat(this.value);
        updateLightPosition(lightIndex);
        render();
    };

    //light ambient
    inputLightAmbientR.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('ambient').r = parseFloat(this.value);
        render();
    };

    inputLightAmbientG.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('ambient').g = parseFloat(this.value);
        render();
    };

    inputLightAmbientB.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('ambient').b = parseFloat(this.value);
        render();
    };

    //light diffuse
    inputLightDiffuseR.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('diffuse').r = parseFloat(this.value);
        render();
    };

    inputLightDiffuseG.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('diffuse').g = parseFloat(this.value);
        render();
    };

    inputLightDiffuseB.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('diffuse').b = parseFloat(this.value);
        render();
    };

    //light diffuse
    inputLightSpecularR.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('specular').r = parseFloat(this.value);
        render();
    };

    inputLightSpecularG.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('specular').g = parseFloat(this.value);
        render();
    };

    inputLightSpecularB.oninput = function () {

        var light = lightArray[lightIndex].light;

        light.get('specular').b = parseFloat(this.value);
        render();
    };

    //eye position input
    inputEyePositionX.oninput = function () {

        eye.get('position').x = parseFloat(this.value);
        updateEyeLocation();
        render();
    };

    inputEyePositionY.oninput = function () {

        eye.get('position').y = parseFloat(this.value);
        updateEyeLocation();
        render();
    };

    inputEyePositionZ.oninput = function () {

        eye.get('position').z = parseFloat(this.value);
        updateEyeLocation();
        render();
    };
}

window.onload = function(){

    try {

        init();
        setupInput();
        render();
    }
    catch (e) {
        console.log(e);
    }
};

