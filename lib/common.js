
var common = (function() {

    function isUndefined(v){
        return (typeof(v) === "undefined");
    }

    return {

        hasValue: function(v){
            return !isUndefined(v) && v !== null;
        },

        isUndefined: function(v){
            return isUndefined(v);
        }
    };

}());

var glUtil = (function(){

    return {

        pointToArray: function(point){
            return [point.x, point.y, point.z];
        },

        arrayToPoint: function(array){
            return {x: array[0], y: array[1], z: array[2]};
        },

        colorToArray: function(color){
            return [color.r, color.g, color.b, color.a];
        },

        arrayToColor: function(array){
            return {r: array[0], g: array[1], b: array[2], a: array[3]};
        }
    };
}());

var mathUtil = (function() {

    return {

        sinDeg: function (deg) {
            return Math.sin(deg * Math.PI / 180);
        },

        cosDeg: function (deg) {
            return Math.cos(deg * Math.PI / 180);
        },

        tanDeg: function (deg) {
            return Math.tan(deg * Math.PI / 180);
        }
    };

}());

function Point(config){

    if (!( this instanceof Point)) {
        return new Point(config);
    }

    var x = 0.0,
        y = 0.0,
        z = 0.0;

    set(config);

    function get(key) {

        var value = null;

        if(key === "x"){
            value = x;
        }

        if(key === "y"){
            value = y;
        }

        if(key === "z"){
            value = z;
        }

        return value;
    }

    function set(config) {

        if (config !== undefined) {

            if (config.x !== undefined) {
                x = config.x;
            }

            if (config.y !== undefined) {
                y = config.y;
            }

            if (config.z !== undefined) {
                z = config.z;
            }
        }
    }

    this.key = function(key){
        return get(key);
    };

    this.set = function(config){
        set(config);
    };

    this.toArray = function(){
        return [x, y, z];
    };
}

function Color(config){

    if (!( this instanceof Color)) {
        return new Color(config);
    }

    var r = 0.0,
        g = 0.0,
        b = 0.0,
        a = 1.0;

    set(config);

    function cutHex(h) {
        return (h.charAt(0)==='#') ? h.substring(1,7 ) : h;
    }

    function hexToR(h) {
        return parseInt((cutHex(h)).substring(0, 2), 16);
    }

    function hexToG(h) {
        return parseInt((cutHex(h)).substring(2, 4), 16);
    }

    function hexToB(h) {
        return parseInt((cutHex(h)).substring(4, 6), 16);
    }

    function convertRgb256(r256, g256, b256){

        r = r256 / 255;
        g = g256 / 255;
        b = b256 / 255;
    }

    function set(config){

        if(config !== undefined){

            if(config.r !== undefined){
                r = config.r;
            }

            if(config.g !== undefined){
                g = config.g;
            }

            if(config.b !== undefined){
                b = config.b;
            }

            if(config.a !== undefined){
                a = config.a;
            }
        }
    }

    this.convertHex = function(h) {

        var r256 = hexToR(h),
            g256 = hexToG(h),
            b256 = hexToB(h);

        convertRgb256(r256, g256, b256);
    };

    this.convertRgb256 = function(r256, g256, b256){
        convertRgb256(r256, g256, b256);
    };

    this.set = function(config){
        set(config);
    };

    this.toObj = function(){
        return {r: r, g: g, b: b, a: a};
    };

    this.toArray = function(){
        return [r, g, b, a];
    };

}

function Frustum(config) {

    if (!( this instanceof Frustum)) {
        return new Frustum(config);
    }

    var left = -2.0,
        right = 2.0,
        bottom = -2.0,
        top = 2.0,
        near = -10.0,
        far = 10.0;

    set(config);

    function set(config){

        if(config !== undefined){

            if(config.left !== undefined){
                left = config.left;
            }

            if(config.right !== undefined){
                right = config.right;
            }

            if(config.top !== undefined){
                top = config.top;
            }

            if(config.bottom !== undefined){
                bottom = config.bottom;
            }

            if(config.near !== undefined){
                near = config.near;
            }

            if(config.far !== undefined){
                far = config.far;
            }
        }
    }

    this.getOrthoProjectionMatrix = function(){
        return ortho( left, right, bottom, top, near, far);
    };

    this.set = function(config){
        set(config);
    };
}

function Eye (config){

    if (!( this instanceof Eye)) {
        return new Eye(config);
    }

    var position = {x: 0.0, y: 0.0, z: 1.0},
        at = {x: 0.0, y: 0.0, z: 0.0},
        up = {x: 0.0, y: 1.0, z: 0.0};

    set(config);

    function get(key){

        var value = null;

        if(key === "position"){
            value = position;
        }

        if(key === "at"){
            value = at;
        }

        if(key === "up"){
            value = up;
        }

        return value;
    }

    function set(config){

        if(config !== undefined){

            if(config.position !== undefined){
                position = config.position;
            }

            if(config.at !== undefined){
                at = config.at;
            }

            if(config.up !== undefined){
                up = config.up;
            }

            if(config.radius !== undefined){
                radius = config.radius;
            }
        }
    }

    this.getLookAt = function(){
        return lookAt(glUtil.pointToArray(position), glUtil.pointToArray(at), glUtil.pointToArray(up) );
    };

    this.set = function(config){
        set(config);
    };

    this.get = function(key){
        return get(key);
    };
}

function Light (config) {

    if (!( this instanceof Light)) {
        return new Light(config);
    }

    var position = {x: 0.0, y: 0.0, z: 0.0},
        ambient = {r: 0.2, g: 0.2, b: 0.2, a: 1.0},
        diffuse = {r: 1.0, g: 1.0, b: 1.0, a: 1.0},
        specular = {r: 0.5, g: 0.5, b: 0.5, a: 1.0};

    set(config);

    function get(key){

        var value = null;

        if(key === "position"){
            value = position;
        }

        if(key === "ambient"){
            value = ambient;
        }

        if(key === "diffuse"){
            value = diffuse;
        }

        if(key === "specular"){
            value = specular;
        }

        return value;
    }

    function set(config){

        if(config !== undefined){

            if(config.position !== undefined){
                position = config.position;
            }

            if(config.ambient !== undefined){
                ambient = config.ambient;
            }

            if(config.diffuse !== undefined){
                diffuse = config.diffuse;
            }

            if(config.specular !== undefined){
                specular = config.specular;
            }
        }
    }

    this.set = function(config){
        set(config);
    };

    this.get = function(key){
        return get(key);
    };
}

function Material (config) {

    if (!( this instanceof Material)) {
        return new Material(config);
    }

    var ambient,
        diffuse,
        specular,
        shininess;

    set(config);

    function get(key){

        var value = null;

        if(key === "ambient"){
            value = ambient;
        }

        if(key === "diffuse"){
            value = diffuse;
        }

        if(key === "specular"){
            value = specular;
        }

        if(key === "shininess"){
            value = shininess;
        }

        return value;
    }

    function set(config){

        if(config !== undefined){

            if(config.ambient !== undefined){
                ambient = config.ambient;
            }

            if(config.diffuse !== undefined){
                diffuse = config.diffuse;
            }

            if(config.specular !== undefined){
                specular = config.specular;
            }

            if(config.shininess !== undefined){
                shininess = config.shininess;
            }
        }
    }

    this.set = function(config){
        set(config);
    };

    this.get = function(key){
        return get(key);
    };

    this.calculateLight = function(light){

        return {
            ambient: mult(glUtil.colorToArray(light.get("ambient")), ambient),
            diffuse: mult(glUtil.colorToArray(light.get("diffuse")), diffuse),
            specular: mult(glUtil.colorToArray(light.get("specular")), specular)
        };
    };
}

var getMaterial = function(name){

    var material;

    switch(name) {

        case "emerald":
            material = {
                ambient: [0.0215, 0.1745, 0.0215, 1.0],
                diffuse: [0.07568, 0.61424, 0.07568, 1.0],
                specular: [0.633, 0.727811, 0.633, 1.0],
                shininess: 0.6
            };
            break;

        case "jade":
            material = {
                ambient: [0.135, 0.2225, 0.1575, 1.0],
                diffuse: [0.54, 0.89, 0.63, 1.0],
                specular: [0.316228, 0.316228, 0.316228, 1.0],
                shininess: 0.1
            };
            break;

        case "obsidian":
            material = {
                ambient: [0.05375, 0.05, 0.06625, 1.0],
                diffuse: [0.18275, 0.17, 0.22525, 1.0],
                specular: [0.332741, 0.328634, 0.346435, 1.0],
                shininess: 0.3
            };
            break;

        case "pearl":
            material = {
                ambient: [0.25, 0.20725, 0.20725, 1.0],
                diffuse: [1.0, 0.829, 0.829, 1.0],
                specular: [0.296648, 0.296648, 0.296648, 1.0],
                shininess: 0.088
            };
            break;

        case "ruby":
            material = {
                ambient: [0.1745, 0.01175, 0.01175, 1.0],
                diffuse: [0.61424, 0.04136, 0.04136, 1.0],
                specular: [0.727811, 0.626959, 0.626959, 1.0],
                shininess: 0.6
            };
            break;

        case "turquoise":
            material = {
                ambient: [0.1, 0.18725, 0.1745, 1.0],
                diffuse: [0.396, 0.74151, 0.69102, 1.0],
                specular: [0.297254, 0.30829, 0.306678, 1.0],
                shininess: 0.1
            };
            break;

        case "brass":
            material = {
                ambient: [0.329412, 0.223529, 0.027451, 1.0],
                diffuse: [0.780392, 0.568627, 0.113725, 1.0],
                specular: [0.992157, 0.941176, 0.807843, 1.0],
                shininess: 0.21794872
            };
            break;

        case "bronze":
            material = {
                ambient: [0.2125, 0.1275, 0.054, 1.0],
                diffuse: [0.714, 0.4284, 0.18144, 1.0],
                specular: [0.393548, 0.271906, 0.166721, 1.0],
                shininess: 0.2
            };
            break;

        case "chrome":
            material = {
                ambient: [0.25, 0.25, 0.25, 1.0],
                diffuse: [0.4, 0.4, 0.4, 1.0],
                specular: [0.774597, 0.774597, 0.774597, 1.0],
                shininess: 0.6
            };
            break;

        case "copper":
            material = {
                ambient: [0.19125, 0.0735, 0.0225, 1.0],
                diffuse: [0.7038, 0.27048, 0.0828, 1.0],
                specular: [0.256777, 0.137622, 0.086014, 1.0],
                shininess: 0.1
            };
            break;

        case "gold":
            material = {
                ambient: [0.24725, 0.1995, 0.0745, 1.0],
                diffuse: [0.75164, 0.60648, 0.22648, 1.0],
                specular: [0.628281, 0.555802, 0.366065, 1.0],
                shininess: 0.4
            };
            break;

        case "silver":
            material = {
                ambient: [0.19225, 0.19225, 0.19225, 1.0],
                diffuse: [0.50754, 0.50754, 0.50754, 1.0],
                specular: [0.508273, 0.508273, 0.508273, 1.0],
                shininess: 0.4
            };
            break;

        case "black_plastic":
            material = {
                ambient: [0.0, 0.0, 0.0, 1.0],
                diffuse: [0.01, 0.01, 0.01, 1.0],
                specular: [0.50, 0.50, 0.50, 1.0],
                shininess: 0.25
            };
            break;

        case "cyan_plastic":
            material = {
                ambient: [0.0, 0.1, 0.06, 1.0],
                diffuse: [0.0, 0.50980392, 0.50980392, 1.0],
                specular: [0.50196078, 0.50196078, 0.50196078, 1.0],
                shininess: 0.25
            };
            break;

        case "green_plastic":
            material = {
                ambient: [0.0, 0.0, 0.0, 1.0],
                diffuse: [0.1, 0.35, 0.1, 1.0],
                specular: [0.45, 0.55, 0.45, 1.0],
                shininess: 0.25
            };
            break;

        case "red_plastic":
            material = {
                ambient: [0.0, 0.0, 0.0, 1.0],
                diffuse: [0.5, 0.0, 0.0, 1.0],
                specular: [0.7, 0.6, 0.6, 1.0],
                shininess: 0.25
            };
            break;
    }

    return material;
};

