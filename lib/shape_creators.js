
var ShapeCreator = (function() {

    function Shape(config) {

        var pointArray = [],
            colorArray = [],
            normalArray = [],

            translation = {x: 0.0, y: 0.0, z: 0.0},
            rotation = {x: 0.0, y: 0.0, z: 0.0},
            scale = {x: 1.0, y: 1.0, z: 1.0},

            material = new Material();

        set(config);

        function get(key){

            var value = null;

            if(key === "pointArray") {
                value = pointArray;
            }

            if(key === "colorArray") {
                value = colorArray;
            }

            if(key === "normalArray") {
                value = normalArray;
            }

            if(key === "translation") {
                value = translation;
            }

            if(key === "rotation") {
                value = rotation;
            }

            if(key === "scale") {
                value = scale;
            }

            if(key === "material") {
                value = material;
            }

            return value;
        }

        function set(config){

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.pointArray)){
                    pointArray = config.pointArray;
                }

                if(!common.isUndefined(config.colorArray)){
                    colorArray = config.colorArray;
                }

                if(!common.isUndefined(config.normalArray)){
                    normalArray = config.normalArray;
                }

                if(!common.isUndefined(config.translation)){
                    translation = config.translation;
                }

                if(!common.isUndefined(config.rotation)){
                    rotation = config.rotation;
                }

                if(!common.isUndefined(config.scale)){
                    scale = config.scale;
                }
            }
        }

        this.get = function(key){
            return get(key);
        };

        this.set = function(config){
            return set(config);
        };

        this.setColor = function(color){

            var i = 0;

            colorArray = [];

            for(i = 0; i < pointArray.length; i++) {
                colorArray.push(color);
            }
        };

    }

    return {

        getInstance: function(config) {
            return new Shape(config);
        }
    };

}());

var CubeCreator = (function() {

    function Cube(config, parent) {

        function get(key){

            var value = null;

            if(key === "type") {
                value = "cube";
            }

            return value;
        }

        function set(config){

        }

        this.get = function(key){

            var value = get(key);

            if(value === null){
                value = parent.get(key);
            }

            return value;
        };

        this.set = function(config){

            parent.set(config);

            set(config);
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeCreator.getInstance(config);
            Cube.prototype = parent;
            return new Cube(config, parent);
        }
    };

}());


var ConeCreator = (function() {

    function Cone(config, parent) {

        var bodyCount = 0,
            bottomOffset = 0,
            bottomCount = 0;

        set(config);

        function get(key){

            var value = null;

            if(key === "type") {
                value = "cone";
            }

            if(key === "bodyCount") {
                value = bodyCount;
            }

            if(key === "bottomOffset") {
                value = bottomOffset;
            }

            if(key === "bottomCount") {
                value = bottomCount;
            }

            return value;
        }

        function set(config){

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.bodyCount)){
                    bodyCount = config.bodyCount;
                }

                if(!common.isUndefined(config.bottomOffset)){
                    bottomOffset = config.bottomOffset;
                }

                if(!common.isUndefined(config.bottomCount)){
                    bottomCount = config.bottomCount;
                }
            }
        }

        this.set = function(config){

            parent.set(config);

            set(config);
        };

        this.get = function(key){

            var value = get(key);

            if(value === null){
                value = parent.get(key);
            }

            return value;
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeCreator.getInstance(config);
            Cone.prototype = parent;
            return new Cone(config, parent);
        }
    };

}());

var CylinderCreator = (function() {

    function Cylinder(config, parent) {

        var bodyCount = 0,
            topOffset = 0,
            topCount = 0,
            bottomOffset = 0,
            bottomCount = 0;

        set(config);

        function get(key){

            var value = null;

            if(key === "type") {
                value = "cylinder";
            }

            if(key === "bodyCount") {
                value = bodyCount;
            }

            if(key === "topOffset") {
                value = topOffset;
            }

            if(key === "topCount") {
                value = topCount;
            }

            if(key === "bottomOffset") {
                value = bottomOffset;
            }

            if(key === "bottomCount") {
                value = bottomCount;
            }

            return value;
        }

        function set(config){

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.bodyCount)){
                    bodyCount = config.bodyCount;
                }

                if(!common.isUndefined(config.topOffset)){
                    topOffset = config.topOffset;
                }

                if(!common.isUndefined(config.topCount)){
                    topCount = config.topCount;
                }

                if(!common.isUndefined(config.bottomOffset)){
                    bottomOffset = config.bottomOffset;
                }

                if(!common.isUndefined(config.bottomCount)){
                    bottomCount = config.bottomCount;
                }
            }
        }

        this.set = function(config){

            parent.set(config);

            set(config);
        };

        this.get = function(key){

            var value = get(key);

            if(value === null){
                value = parent.get(key);
            }

            return value;
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeCreator.getInstance(config);
            Cylinder.prototype = parent;
            return new Cylinder(config, parent);
        }
    };

}());


var SphereCreator = (function() {

    function Sphere(config, parent) {

        var bodyCount = 0,
            topOffset = 0,
            topCount = 0,
            bottomOffset = 0,
            bottomCount = 0,
            textureCoordinateArray = [];

        set(config);

        function get(key){

            var value = null;

            if(key === "type") {
                value = "sphere";
            }

            if(key === "bodyCount") {
                value = bodyCount;
            }

            if(key === "topOffset") {
                value = topOffset;
            }

            if(key === "topCount") {
                value = topCount;
            }

            if(key === "bottomOffset") {
                value = bottomOffset;
            }

            if(key === "bottomCount") {
                value = bottomCount;
            }

            if(key === "textureCoordinateArray") {
                value = textureCoordinateArray;
            }

            return value;
        }

        function set(config){

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.bodyCount)){
                    bodyCount = config.bodyCount;
                }

                if(!common.isUndefined(config.topOffset)){
                    topOffset = config.topOffset;
                }

                if(!common.isUndefined(config.topCount)){
                    topCount = config.topCount;
                }

                if(!common.isUndefined(config.bottomOffset)){
                    bottomOffset = config.bottomOffset;
                }

                if(!common.isUndefined(config.bottomCount)){
                    bottomCount = config.bottomCount;
                }

                if(!common.isUndefined(config.textureCoordinateArray)){
                    textureCoordinateArray = config.textureCoordinateArray;
                }
            }
        }

        this.set = function(config){

            parent.set(config);

            set(config);
        };

        this.get = function(key){

            var value = get(key);

            if(value === null){
                value = parent.get(key);
            }

            return value;
        };

    }

    return {

        getInstance: function(config) {

            var parent = ShapeCreator.getInstance(config);
            Sphere.prototype = parent;
            return new Sphere(config, parent);
        }
    };

}());


var ShapeFactoryCreator = (function() {

    function ShapeFactory(config) {

        var color = [0.6627450980392157, 0.6627450980392157, 0.6627450980392157, 1.0];

        function set(config){

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.color)){
                    color = config.color;
                }
            }
        }

        function get(key){

            var value = null;

            if(key === 'color'){
                value = color;
            }

            return value;
        }

        this.set = function(config){
            set(config);
        };

        this.get = function(key){
            return get(key);
        };
    }

    return {

        getInstance: function(config) {
            return new ShapeFactory(config);
        }
    };

}());

var ConeFactoryCreator = (function() {

    function ConeFactory(config, parent) {

        var height = 0.5,
            radius = 0.15,
            divisions = 50,
            twoPI = Math.PI * 2,
            slice = twoPI / divisions;

        hHalf = height / 2;

        this.buildShape = function (config) {

            var color = parent.get('color'),
                t1, t2,
                point0,
                point1,
                point2,
                normal,
                pointArray = [],
                colorArray = [],
                normalArray = [],
                bodyCount = 0,
                bottomCount = 0,
                bottomOffset = 0;

            //vertex
            point0 = vec3(0.0, hHalf, 0.0);
            pointArray.push(point0);
            colorArray.push(color);
            normalArray.push([0, 1, 0]);

            for (i = 0; i <= divisions; i++) {

                point1 = vec3(radius * Math.cos(i * slice), -hHalf, radius * Math.sin(i * slice));
                pointArray.push(point1);
                colorArray.push(color);
            }

            //calculate normals
            for (i = 1; i < pointArray.length; i++) {

                point1 = pointArray[i];
                if((i + 1) < pointArray.length){
                    point2 = pointArray[i + 1];
                }
                else {
                    point2 = pointArray[2];
                }

                t1 = subtract(point2, point0);
                t2 = subtract(point1, point0);

                normal = cross(t1, t2);
                normal = normalize(normal);

                normalArray.push(normal);
            }

            bodyCount = pointArray.length;
            bottomOffset = pointArray.length;

            //bottom surface
            normal = [0, -1, 0];
            pointArray.push(vec3(0.0, -hHalf, 0.0));
            colorArray.push(color);
            normalArray.push(normal);

            for (i = 0; i <= divisions; i++) {

                pointArray.push(vec3(radius * Math.cos(i * slice), -hHalf, radius * Math.sin(i * slice)));
                colorArray.push(color);
                normalArray.push(normal);
            }

            bottomCount = pointArray.length - bottomOffset;

            return ConeCreator.getInstance({pointArray: pointArray, colorArray: colorArray, normalArray: normalArray, bodyCount: bodyCount, bottomOffset: bottomOffset, bottomCount: bottomCount});
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeFactoryCreator.getInstance(config);
            ConeFactory.prototype = parent;
            return new ConeFactory(config, parent);
        }
    };

}());

var CylinderFactoryCreator = (function() {

    function CylinderFactory(config, parent){

        var height = 0.5,
            hHalf,
            radius = 0.15,
            divisions = 50,
            twoPI = Math.PI * 2,
            slice = twoPI / divisions;

        hHalf = height / 2;

        this.buildShape = function(config){

            var i = 0,
                point,
                point0, point1, point2,
                normal,
                color = parent.get('color'),
                pointArray = [],
                colorArray = [],
                normalArray = [],
                bodyCount = 0,
                topCount = 0,
                topOffset = 0,
                bottomCount = 0,
                bottomOffset = 0;

            for(i = 0; i <= divisions; i++) {

                point = vec3(radius * Math.cos(i *  slice), -hHalf, radius * Math.sin(i * slice));

                pointArray.push(point);
                colorArray.push(color);

                point = vec3(radius * Math.cos(i *  slice), hHalf, radius * Math.sin(i * slice));

                pointArray.push(point);
                colorArray.push(color);
            }

            //calculate normals
            for (i = 0; i < pointArray.length; i = i + 2) {

                point1 = pointArray[i];
                point2 = pointArray[i + 1];
                if((i + 3) < pointArray.length){
                    point0 = pointArray[i + 3];
                }
                else {
                    point0 = pointArray[3];
                }

                t1 = subtract(point1, point0);
                t2 = subtract(point2, point0);

                normal = cross(t1, t2);
                normal = normalize(normal);

                normalArray.push(normal);
                normalArray.push(normal);
            }

            bodyCount = pointArray.length;
            topOffset = pointArray.length;

            //top surface
            point = vec3(0.0, hHalf, 0.0);
            pointArray.push(point);
            colorArray.push(color);

            normal = [0.0, 1.0, 0.0];
            normalArray.push(normal);

            for (i = 0; i <= divisions; i++) {

                pointArray.push(vec3(radius * Math.cos(i * slice), hHalf, radius * Math.sin(i * slice)));
                colorArray.push(color);
                normalArray.push(normal);
            }

            topCount = pointArray.length - topOffset;
            bottomOffset = pointArray.length;

            //bottom surface
            point = vec3(vec3(0.0, -hHalf, 0.0));
            pointArray.push(point);
            colorArray.push(color);

            normal = [0.0, -1.0, 0.0];
            normalArray.push(normal);

            for (i = 0; i <= divisions; i++) {

                pointArray.push(vec3(radius * Math.cos(i * slice), -hHalf, radius * Math.sin(i * slice)));
                colorArray.push(color);
                normalArray.push(normal);
            }

            bottomCount = pointArray.length - bottomOffset;

            return CylinderCreator.getInstance({pointArray: pointArray, colorArray: colorArray, normalArray: normalArray, bodyCount: bodyCount, topOffset: topOffset, topCount: topCount, bottomOffset: bottomOffset, bottomCount: bottomCount});
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeFactoryCreator.getInstance(config);
            CylinderFactory.prototype = parent;
            return new CylinderFactory(config, parent);
        }
    };

}());


var SphereFactoryCreator = (function() {

    function SphereFactory(config, parent){

        var radius = 0.25,
            divisions = 25.0,
            twoPI = Math.PI * 2,
            thetaSlice,
            phiSlice,
            texSlice;

        set(config);

        function set(config){

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.radius)){
                    radius = config.radius;
                }

                if(!common.isUndefined(config.divisions)){
                    divisions = config.divisions;
                }
            }

            thetaSlice = twoPI / divisions;
            phiSlice = Math.PI / divisions;
            texSlice = 1.0 / divisions;
        }

        this.set = function(config){

            parent.set(config);
            set(config);
        };

        this.buildShape = function(config){

            var i = 0,
                s = 1.0, t = 1.0,
                phi = 0.0,
                theta = 0.0,
                point,
                color = parent.get('color'),
                pointArray = [],
                colorArray = [],
                normalArray = [],
                textureCoordinateArray = [],
                bodyCount = 0,
                topCount = 0,
                topOffset = 0,
                bottomCount = 0,
                bottomOffset = 0,
                bodyTopEdge = phiSlice,
                bodyBottomEdge = Math.PI - phiSlice;

            for (phi = 0; phi < Math.PI; phi = phi + phiSlice) {

                if(phi != Math.PI && (phi + phiSlice) > Math.PI){
                    phi = Math.PI - phiSlice;
                }

                s = 1.0;

                for(theta = 0.0; theta <= twoPI; theta = theta + thetaSlice) {

                    if(theta != twoPI && (theta + thetaSlice) > twoPI){
                        theta = twoPI - thetaSlice;
                    }

                    point = vec3(radius*Math.sin(phi)*Math.cos(theta), radius*Math.cos(phi), radius*Math.sin(phi)*Math.sin(theta));

                    if((t - texSlice) < 0.0){
                        t = 0.0 + texSlice;
                    }

                    pointArray.push(point);
                    colorArray.push(color);
                    normalArray.push(point);
                    textureCoordinateArray.push([s, t]);

                    point = vec3(radius*Math.sin(phi + phiSlice)*Math.cos(theta), radius*Math.cos(phi + phiSlice), radius*Math.sin(phi + phiSlice)*Math.sin(theta));

                    pointArray.push(point);
                    colorArray.push(color);
                    normalArray.push(point);
                    textureCoordinateArray.push([s, t - texSlice]);

                    s = s - texSlice;
                    if(s < 0.0){
                        s = 0.0;
                    }
                }

                t = t - texSlice;
                if(t < 0.0){
                    t = 0.0;
                }

            }

            bodyCount = pointArray.length;

            return SphereCreator.getInstance({pointArray: pointArray, colorArray: colorArray, normalArray: normalArray, textureCoordinateArray: textureCoordinateArray, bodyCount: bodyCount, topOffset: topOffset, topCount: topCount, bottomOffset: bottomOffset, bottomCount: bottomCount});
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeFactoryCreator.getInstance(config);
            SphereFactory.prototype = parent;
            return new SphereFactory(config, parent);
        }
    };

}());


var CubeFactoryCreator = (function() {

    function CubeFactory(config, parent){

        var width = 0.3,
            wHalf,
            height = 0.3,
            hHalf,
            depth = 0.3,
            dHalf,
            center = vec3( 0.0, 0.0, 0.0),
            vertexArray = [],
            normArray = [
                [0.0, 0.0, 1.0],
                [1.0, 0.0, 0.0],
                [0.0, -1.0, 0.0],
                [0.0, 1.0, 0.0],
                [0.0, 0.0, -1.0],
                [-1.0, 0.0, 0.0]
            ],
            quadArray = [
                [1, 0, 3, 2],
                [2, 3, 7, 6],
                [3, 0, 4, 7],
                [6, 5, 1, 2],
                [4, 5, 6, 7],
                [5, 4, 0, 1]
            ];

        set(config);

        wHalf = width/2.0;
        hHalf = height/2.0;
        dHalf = depth/2.0;

        vertexArray = [
            vec3( -wHalf, -hHalf,  dHalf ),
            vec3( -wHalf,  hHalf,  dHalf ),
            vec3(  wHalf,  hHalf,  dHalf ),
            vec3(  wHalf, -hHalf,  dHalf ),
            vec3( -wHalf, -hHalf, -dHalf ),
            vec3( -wHalf,  hHalf, -dHalf ),
            vec3(  wHalf,  hHalf, -dHalf ),
            vec3(  wHalf, -hHalf, -dHalf )
        ];

        function set(config){

            parent.set(config);

            if(!common.isUndefined(config)){

                if(!common.isUndefined(config.width)){
                    width = config.width;
                }

                if(!common.isUndefined(config.height)){
                    height = config.height;
                }

                if(!common.isUndefined(config.depth)){
                    depth = config.depth;
                }
            }
        }

        this.buildShape = function(config){

            var i = 0,
                j = 0,
                quad,
                t1, t2,
                normal,
                indices,
                color = parent.get('color'),
                pointArray = [],
                colorArray = [],
                normalArray = [];

            for(i = 0; i < quadArray.length; i++){

                quad = quadArray[i];
                indices = [ quad[0], quad[1], quad[2], quad[0], quad[2], quad[3] ];

                //t1 = subtract(vertexArray[quad[1]], vertexArray[quad[0]]);
                //t2 = subtract(vertexArray[quad[2]], vertexArray[quad[1]]);
                //normal = cross(t1, t2);
                //normal = vec3(normal);
                //normal = normalize(normal);

                normal = normArray[i];

                for ( j = 0; j < indices.length; ++j ) {

                    pointArray.push(vertexArray[indices[j]]);
                    colorArray.push(color);
                    normalArray.push(normal);
                }

            }

            return CubeCreator.getInstance({pointArray: pointArray, colorArray: colorArray, normalArray: normalArray});
        };

        this.set = function(config){
            set(config);
        };
    }

    return {

        getInstance: function(config) {

            var parent = ShapeFactoryCreator.getInstance(config);
            CubeFactory.prototype = parent;
            return new CubeFactory(config, parent);
        }
    };

}());