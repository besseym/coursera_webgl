

function Circle(config){

    var radius, divisions;

    if (!( this instanceof Circle)) {
        return new Circle(config);
    }

    radius = 1.0;
    divisions = 30;

    //construct properties from config
    set(config);

    this.getVertices = function(){

        var vertices = [],
            twoPI = Math.PI * 2,
            slice = twoPI / divisions;

        for(i = 0; i < divisions; i++) {

            vertices.push((radius * Math.cos(i *  slice)), (radius * Math.sin(i * slice)));
        }

        console.log(vertices.length);

        return vertices;
    };

    this.getVertexCount = function(){
        return divisions;
    };

    this.getDrawMode = function(gl){

        return gl.TRIANGLE_FAN;
    };

    this.set = function(config) {
        set(config);
    };

    function set(config){

        if(config){

            if(config.radius){
                radius = config.radius;
            }

            if(config.divisions){
                divisions = config.divisions;
            }
        }
    }
}

function Triangle(config){

    var height, base;
}

function TriangleEquilateral(config){

    var height, bHalf;

    if (!( this instanceof TriangleEquilateral)) {
        return new TriangleEquilateral(config);
    }

    height = 0.5;
    bHalf = height / MathUtil.tanDeg(60);

    //construct properties from config
    set(config);

    this.getVertices = function(){

        var hs = height / 3,
            hs2 = hs * 2;

        return [-bHalf, -hs, 0.0, hs2, bHalf, -hs];
    };

    this.getVertexCount = function(){
        return 3;
    };

    this.getDrawMode = function(gl){

        return gl.TRIANGLES;
    };

    this.set = function(config) {
        set(config);
    };

    function set(config){

        if(config){

            if(config.height){
                height = config.height;
            }
        }
    }
}


function Rectange(config){

    var width, height;

    if (!( this instanceof Rectange)) {
        return new Rectange(config);
    }

    width = 1.0;
    height = 1.0;

    //construct properties from config
    set(config);

    this.getVertices = function(){

        var w = width * 0.5,
            h = height * 0.5;

        return [-w, -h, -w, h, w, h, w, -h];
    };

    this.getVertexCount = function(){
        return 4;
    };

    this.getDrawMode = function(gl){

        return gl.TRIANGLE_FAN;
    };

    this.set = function(config) {
        set(config);
    };

    function set(config){

        if(config){

            if(config.width){
                width = config.width;
            }

            if(config.height){
                height = config.height;
            }
        }
    }
}


function getShape(shape, config){

    if(shape === "rect"){
        return new Rectange(config);
    }
    else if(shape === "circle"){
        return new Circle(config);
    }
    else if(shape === "triangle_equilateral"){
        return new TriangleEquilateral(config);
    }

}