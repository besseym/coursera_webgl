

var MathUtil = {

    tanDeg: function (deg) {

        var rad = deg * Math.PI / 180;
        return Math.tan(rad);
    }
};